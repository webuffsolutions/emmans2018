@extends('layouts.app-shiftSelection')

@section('content')
	
	<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>

            <!-- Panels -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect">
                        <div class="icon bg-light-green" style="opacity:0.8;">
                            <i class="material-icons">shopping_cart</i>
                        </div>
                        <div class="content">
                            <div class="text">
                                <h4><a href="{{ route('sell') }}" style="text-decoration:none;">Shopping Cart</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect">
                        <div class="icon bg-light-green" style="opacity:0.8;">
                            <i class="material-icons">add_to_photos</i>
                        </div>
                        <div class="content">
                            <div class="text">
                                <h4><a href="{{ route('stocks.index') }}" style="text-decoration:none;">Restock</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect">
                        <div class="icon bg-light-green" style="opacity:0.8;">
                            <i class="material-icons">touch_app</i>
                        </div>
                        <div class="content">
                            <div class="text">
                                <h4><a href="{{ route('transactions.index') }}" style="text-decoration:none;">Transactions</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Col-lg-3-->
           
        </div>
    </section>

@stop