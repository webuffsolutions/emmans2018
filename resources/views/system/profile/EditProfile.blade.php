@extends('layouts.app-template')

@section('content')
	
	<section class="content">
    <div class="container-fluid">

	 	<div class="row clearfix">
	        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
	        	@include('includes.errors')

	            <div class="card">
	                <div class="header bg-light-green" style="opacity:0.8;"">
	                    <h2>
	                        <i>Edit Profile</i>
	                        <small>*you can change your profile settings here.</small>
	                    </h2>
	                    <ul class="header-dropdown m-r--5">
	                        <li class="dropdown">
	                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	                                <i class="material-icons">more_vert</i>
	                            </a>
	                            <ul class="dropdown-menu pull-right">
	                                <li><a href="
											@if (Auth::user()->role->id == App\User::IS_ADMIN)
				                                {{ route('AdminDashboard') }}
				                            @elseif (Auth::user()->role->id == App\User::IS_CASHIER)
				                                {{ route('CashierDashboard') }}
				                            @endif
	                                	">return to dashboard</a></li>
	                            </ul>
	                        </li>
	                    </ul>
	                </div>
	                <div class="body">
	                    <form action="#" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}

	                        <label for="name">Name</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="text" name="name" id="name" class="form-control"  
	                                value="{{ Auth::user()->name }}">
	                            </div>
	                        </div>

	                        <label for="username">Username</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="text" name="username" id="username" class="form-control"
	                                value="{{ Auth::user()->username }}">
	                            </div>
	                        </div>

	                        <label for="old_password">Old Password</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="password" name="old_password" id="old_password" placeholder="******" 
	                                class="form-control"
	                                >
	                            </div>
	                        </div>

	                        <label for="new_password">New Password</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="password" name="new_password" id="new_password" placeholder="******" class="form-control"
	                                >
	                            </div>
	                        </div>

	                        <label for="confirm_password">Confirm New Password</label>
	                        <div class="form-group">
	                            <div class="form-line">
	                                <input type="password" name="confirm_password" id="confirm_password" placeholder="******" class="form-control"
	                                >
	                            </div>
	                            <span id='message'></span>
	                        </div>

	                        	<button type="submit" id="updateProfileBtn" class="btn bg-light-green waves-effect" style="opacity:0.8;">
	                        		Update
	                        	</button>
	                    </form>
	                </div>
	            </div>
	        </div>
        </div>

    </div>
</section>

<script>

    $('#new_password, #confirm_password').on('keyup', function () {
    	if ($('#new_password').val() == '') {
    		$('#message').html('Password not matched.').css('color', 'red');
    		$("#updateProfileBtn").prop("disabled", true);
    		return false;
    	}

    	if ($('#new_password').val() != $('#confirm_password').val()) {
    		$('#message').html('Password not matched.').css('color', 'red');
    		$("#updateProfileBtn").prop("disabled", true);
    		return false;
    	} else {
            $('#message').html('Password matched.').css('color', 'green');
            $("#updateProfileBtn").prop("disabled", false);
            return true;
    	}
    });

</script>

@stop