<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <style media="screen">
       .container {
           font-size: 0;
       }
       .float-alternative {
           font-size: 14px;
           display: inline-block;
           width: 50%;
       }
       .float-alternative .inner-container {
           padding: 10px;
       }
       .float-alternative table {
           width: 100%;
           border: 1px solid black;
       }
   </style> -->
   <style type="text/css">
        th { font-size: 11px;  }
        td { font-size: 10px; }

        table {

              width: 100%;
              white-space: nowrap;
        }
   </style>
</head>
<body>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <center>
                    <h3><i>
                        <b>Emman's Daily Inventory/Sales Report</b>
                    </i></h3>
                </center>

                <table width="1000" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="80%"><b>Date:</b> <i>{{ date('m-d-Y H:i:s A') }}</i></td>
                        <td width="20%"><b>Cashier:</b> <i>
                            @if(!Auth::user())
                                <script type="text/javascript">
                                    window.location = "{ url('user/logout') }";//logout if not authenticated
                                </script>
                            @else
                                {{ Auth::user()->name }}
                            @endif
                        </i></td>
                    </tr>
                </table>
                <hr>

               <div class="container">
               <table width="90%">
                    <tr>
                        <!-- GROUP A -->
                        <td valign="top">
                            <table border="1" style="border-collapse: collapse;">
                                <caption>
                                    <center><h3>ORDER A</h3></center>
                                </caption>

                                <tr>
                                    <th>ITEM</th>
                                    <th>BEG</th>
                                    <th>REQ</th>
                                    <th>END</th>
                                    <th>QTYSOLD</th>
                                    <th>AMOUNT</th>
                                </tr>
                                @foreach($reports as $report)
                                    @if($report->group_name == 'A')
                                        <tr>
                                            <td>{{$report->product_name}}</td>

                                            @foreach($getBegFromPrev as $getBeg)
                                                @if($getBeg->beg_product_id == $report->product_id)
                                                    @if(count($getBeg->beg) > 0)
                                                        <td>{{$getBeg->beg}}</td>
                                                    @else
                                                        <td>0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                            @if($report->RQ == null)
                                                <td>0</td>
                                            @else
                                                <td>{{$report->RQ}}</td>
                                            @endif

                                            <td>{{$report->end}}</td>
                                            
                                            @foreach($reports1 as $report1)
                                                @if($report1->prod_id == $report->product_id)
                                                    @if(count($report1->qty_sold) > 0)
                                                        <td>{{$report1->qty_sold}}</td>
                                                        <td>P{{$report1->total_amount}}</td>
                                                    @else
                                                        <td>0</td>
                                                        <td>P0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                        </tr>
                                    @endif
                                @endforeach
                               
                            </table>
                            <h3>SALES A:</h3>
                        </td>
                        
                        <!-- GROUP B -->
                        <td valign="top">
                            <table border="1" style="border-collapse: collapse;margin-left:10px;">
                                <caption>
                                    <center><h3>ORDER B</h3></center>
                                </caption>

                                <tr>
                                    <th>ITEM</th>
                                    <th>BEG</th>
                                    <th>REQ</th>
                                    <th>END</th>
                                    <th>QTYSOLD</th>
                                    <th>AMOUNT</th>
                                </tr>
                                @foreach($reports as $report)
                                    @if($report->group_name == 'B')
                                        <tr>
                                            <td>{{$report->product_name}}</td>

                                            @foreach($getBegFromPrev as $getBeg)
                                                @if($getBeg->beg_product_id == $report->product_id)
                                                    @if(count($getBeg->beg) > 0)
                                                        <td>{{$getBeg->beg}}</td>
                                                    @else
                                                        <td>0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                            @if($report->RQ == null)
                                                <td>0</td>
                                            @else
                                                <td>{{$report->RQ}}</td>
                                            @endif

                                            <td>{{$report->end}}</td>
                                            
                                            @foreach($reports1 as $report1)
                                                @if($report1->prod_id == $report->product_id)
                                                    @if(count($report1->qty_sold) > 0)
                                                        <td>{{$report1->qty_sold}}</td>
                                                        <td>P{{$report1->total_amount}}</td>
                                                    @else
                                                        <td>0</td>
                                                        <td>P0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                        </tr>
                                    @endif
                                @endforeach
                               
                            </table>
                            <h3 style="margin-left:12px;">SALES B:</h3>
                        </td>
                    </tr>
                    <!-- row 2 -->
                    <tr>
                        <!-- GROUP C -->
                        <td valign="top">
                            <table border="1" style="border-collapse: collapse;">
                                <caption>
                                    <center><h3>ORDER C</h3></center>
                                </caption>

                                <tr>
                                    <th>ITEM</th>
                                    <th>BEG</th>
                                    <th>REQ</th>
                                    <th>END</th>
                                    <th>QTYSOLD</th>
                                    <th>AMOUNT</th>
                                </tr>
                                @foreach($reports as $report)
                                    @if($report->group_name == 'C')
                                        <tr>
                                            <td>{{$report->product_name}}</td>

                                            @foreach($getBegFromPrev as $getBeg)
                                                @if($getBeg->beg_product_id == $report->product_id)
                                                    @if(count($getBeg->beg) > 0)
                                                        <td>{{$getBeg->beg}}</td>
                                                    @else
                                                        <td>0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                            @if($report->RQ == null)
                                                <td>0</td>
                                            @else
                                                <td>{{$report->RQ}}</td>
                                            @endif

                                            <td>{{$report->end}}</td>
                                            
                                            @foreach($reports1 as $report1)
                                                @if($report1->prod_id == $report->product_id)
                                                    @if(count($report1->qty_sold) > 0)
                                                        <td>{{$report1->qty_sold}}</td>
                                                        <td>P{{$report1->total_amount}}</td>
                                                    @else
                                                        <td>0</td>
                                                        <td>P0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                        </tr>
                                    @endif
                                @endforeach
                               
                            </table>
                            <h3>SALES C:</h3>
                        </td>

                        <!-- GROUP D -->
                        <td valign="top">
                            <table border="1" style="border-collapse: collapse;margin-left:10px;">
                                <caption>
                                    <center><h3>ORDER D</h3></center>
                                </caption>

                                <tr>
                                    <th>ITEM</th>
                                    <th>BEG</th>
                                    <th>REQ</th>
                                    <th>END</th>
                                    <th>QTYSOLD</th>
                                    <th>AMOUNT</th>
                                </tr>
                                @foreach($reports as $report)
                                    @if($report->group_name == 'D')
                                        <tr>
                                            <td>{{$report->product_name}}</td>

                                            @foreach($getBegFromPrev as $getBeg)
                                                @if($getBeg->beg_product_id == $report->product_id)
                                                    @if(count($getBeg->beg) > 0)
                                                        <td>{{$getBeg->beg}}</td>
                                                    @else
                                                        <td>0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                            @if($report->RQ == null)
                                                <td>0</td>
                                            @else
                                                <td>{{$report->RQ}}</td>
                                            @endif

                                            <td>{{$report->end}}</td>
                                            
                                            @foreach($reports1 as $report1)
                                                @if($report1->prod_id == $report->product_id)
                                                    @if(count($report1->qty_sold) > 0)
                                                        <td>{{$report1->qty_sold}}</td>
                                                        <td>P{{$report1->total_amount}}</td>
                                                    @else
                                                        <td>0</td>
                                                        <td>P0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                        </tr>
                                    @endif
                                @endforeach
                               
                            </table>
                            <h3 style="margin-left:12px;">SALES D:</h3>
                        </td>
                    </tr>

                    <tr>
                        <!-- GROUP E -->
                        <td valign="top">
                            <table border="1" style="border-collapse: collapse;">
                                <caption>
                                    <center><h3>ORDER E</h3></center>
                                </caption>

                                <tr>
                                    <th>ITEM</th>
                                    <th>BEG</th>
                                    <th>REQ</th>
                                    <th>END</th>
                                    <th>QTYSOLD</th>
                                    <th>AMOUNT</th>
                                </tr>
                                @foreach($reports as $report)
                                    @if($report->group_name == 'E')
                                        <tr>
                                            <td>{{$report->product_name}}</td>

                                            @foreach($getBegFromPrev as $getBeg)
                                                @if($getBeg->beg_product_id == $report->product_id)
                                                    @if(count($getBeg->beg) > 0)
                                                        <td>{{$getBeg->beg}}</td>
                                                    @else
                                                        <td>0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                            @if($report->RQ == null)
                                                <td>0</td>
                                            @else
                                                <td>{{$report->RQ}}</td>
                                            @endif

                                            <td>{{$report->end}}</td>
                                            
                                            @foreach($reports1 as $report1)
                                                @if($report1->prod_id == $report->product_id)
                                                    @if(count($report1->qty_sold) > 0)
                                                        <td>{{$report1->qty_sold}}</td>
                                                        <td>P{{$report1->total_amount}}</td>
                                                    @else
                                                        <td>0</td>
                                                        <td>P0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                        </tr>
                                    @endif
                                @endforeach
                               
                            </table>
                            <h3>SALES E:</h3>
                        </td>

                        <!-- OTHERS -->
                        <td valign="top">
                            <table border="1" style="border-collapse: collapse;margin-left:10px;">
                                <caption>
                                    <center><h3>Others</h3></center>
                                </caption>

                                <tr>
                                    <th>ITEM</th>
                                    <th>BEG</th>
                                    <th>REQ</th>
                                    <th>END</th>
                                </tr>
                                @foreach($reports as $report)
                                    @if($report->group_name == 'Others')
                                        <tr>
                                            <td>{{$report->product_name}}</td>

                                            @foreach($getBegFromPrev as $getBeg)
                                                @if($getBeg->beg_product_id == $report->product_id)
                                                    @if(count($getBeg->beg) > 0)
                                                        <td>{{$getBeg->beg}}</td>
                                                    @else
                                                        <td>0</td>
                                                    @endif
                                                @endif
                                            @endforeach

                                            @if($report->RQ == null)
                                                <td>0</td>
                                            @else
                                                <td>{{$report->RQ}}</td>
                                            @endif

                                            <td>{{$report->end}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                               
                            </table>

                        </td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <!-- SALES -->
                        <td valign="top" width="33%">
                        SALES:
                        <div style="margin-left:-10px;">
                            <ul>
                                @foreach($product_groups as $pg)
                                    @if($pg->group_name !== 'Others')
                                        @if(count($pg->SALES) > 0) 
                                            <li><b>P{{$pg->SALES}}</b></li>
                                        @else
                                            <li>P0</li>
                                        @endif
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        </td>

                        <td valign="top" width="33%">
                            <div>
                                Total: 
                                    @if(count($total_sales->total > 0))
                                        <b>P{{$total_sales->total}}</b>
                                    @else
                                        P0
                                    @endif
                                    <br>
                                    Senior Disc: 
                                    @if(count($total_discount > 0))
                                        <b>P{{$total_discount}}</b>
                                    @else
                                        P0
                                    @endif
                                    <br>
                                    Less expenses:
                                    <br>
                                    Sign cheat:
                            </div>
                        </td>

                        <td valign="top" width="33%">
                            <div>
                                Net Sales:
                                <br>
                                Short:
                                <br>
                                Over:
                                <br>
                                Total Cash:
                                <br>
                                Delivery Sales:
                            </div>
                        </td>
                    </tr>
                </table>
                   
               
                
               
               </div>


                

               <!--  <div class="body">
                  <table>
                      <tr>
                          <td>
                              <table>
                                   <tr>
                                       <th>ITEM</th>
                                       <th>BEG</th>
                                       <th>REQ</th>
                                       <th>END</th>
                                       <th>QTY SOLD</th>
                                       <th>AMOUNT</th>
                                   </tr>
                                   <tr>
                                       <td>A</td>
                                       <td>1</td>
                                       <td>2</td>
                                       <td>3</td>
                                       <td>4</td>
                                       <td>5</td>
                                   </tr>
                              </table>
                          </td>
               
                          <td width="30px;"> </td>
                         
                          <td>
                              <table>
                                   <tr>
                                       <th>ITEM</th>
                                       <th>BEG</th>
                                       <th>REQ</th>
                                       <th>END</th>
                                       <th>QTY SOLD</th>
                                       <th>AMOUNT</th>
                                   </tr>
                                   <tr>
                                       <td>B</td>
                                       <td>1</td>
                                       <td>2</td>
                                       <td>3</td>
                                       <td>4</td>
                                       <td>5</td>
                                   </tr>
                              </table>
                          </td>
               
                      </tr>
                  </table>
               </div>   -->



               <!--  <div class="body" style="overflow-x:auto;">
                   <div class="container">
                       <table style="font-size:15px;">
                           <tr>
                               <td>
                                   <table border="1" style="border-collapse: collapse;">
                                       <caption>
                                           <center><h4>ORDER A</h4></center>
                                       </caption>
               
                                       <tr>
                                           <th>ITEM</th>
                                           <th>BEG</th>
                                           <th>RQ</th>
                                           <th>END</th>
                                           <th>QTY SOLD</th>
                                           <th>AMOUNT</th>
                                       </tr>
               
                                       <tr>
                                           <td>A</td>
                                           <td>1</td>
                                           <td>2</td>
                                           <td>3</td>
                                           <td>4</td>
                                           <td>5</td>
                                       </tr>
               
                                   </table>
                               </td>
               
                               <td style="width:70px;"> </td>
               
                               <td>
                                   <table border="1" style="border-collapse: collapse;">
                                       <caption>
                                           <center><h4>ORDER B</h4></center>
                                       </caption>
               
                                       <tr>
                                           <th>ITEM</th>
                                           <th>BEG</th>
                                           <th>RQ</th>
                                           <th>END</th>
                                           <th>QTY SOLD</th>
                                           <th>AMOUNT</th>
                                       </tr>
               
                                       <tr>
                                           <td>A</td>
                                           <td>1</td>
                                           <td>2</td>
                                           <td>3</td>
                                           <td>4</td>
                                           <td>5</td>
                                       </tr>
               
                                   </table>
                               </td>
                           </tr>
               
               
                       </table>
                   </div>
               </div> -->

            </div>
        </div>
    </div>
    
</body>
</html>