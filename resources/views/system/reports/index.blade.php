@extends('layouts.app-demo')

<link href="{{ asset('css/print.css') }}" rel="stylesheet" media="all">

@section('content')

	<section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">print</i>
                        </div>
                        <div class="content" id="content-header1">
                            <h3>DAILY INVENTORY/SALES REPORT</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="printmoto">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h3>
                            	<b><center>Emman's Daily Inventory/Sales Report</center></b>
                        	</h3>
                        	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                        		<b>Date: <i>{{ date('m-d-Y H:i:s A') }}</i></b>
                        	</div>
                            
                        	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-md-offset-6" id="cashier-placeholder">
                        		<b>Cashier:</b> <i>{{ Auth::user()->name }}</i>
                        	</div>

                        	<div style="height:20px;"></div>

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons" id="dropdown-icon">more_vert</i>
                                    </a>
                                    
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a id="print-reports-btn">
                                                <i class="material-icons" id="print-logo">print</i>
                                                Print Daily Report
                                            </a>
                                        </li>

                                    </ul>
                                    
                                </li>
                            </ul>

                        </div>

                        <div class="body" style="overflow-x:auto;">
                        	@foreach($product_groups as $product_group)
                           	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">		
                           		@if($product_group->group_name !== 'Others')
                           			<center><h4>ORDER {{$product_group->group_name}}</h4></center>
                           		@else
									<center><h4>{{$product_group->group_name}}</h4></center>
                           		@endif
                           		<table>
                           			<thead>
                           				<tr>
                           					<th>ITEM</th>
                           					<th>BEG</th>
                           					<th>RQ</th>
                           					<th>END</th>
                                            @if($product_group->group_name !== 'Others')
                           					    <th>QTY SOLD</th>
                           					    <th>AMOUNT</th>
                                            @endif
                           				</tr>
                           			</thead>
                           			<tbody>
                           				@foreach($reports as $report)
                           					@if($report->group_name == $product_group->group_name)
                           					<tr>
                           						<td>{{$report->product_name}}</td>
                                                

                                                @foreach($getBegFromPrev as $getBeg)
                                                    @if($getBeg->beg_product_id == $report->product_id)
                                                        @if(count($getBeg->beg) > 0)
                           						            <td>{{$getBeg->beg}}</td>
                                                        @else
                                                            <td>0</td>
                                                        @endif
                                                    @endif
                                                @endforeach
                                                
                           						@if($report->RQ == null)
													<td>0</td>
												@else
													<td>{{$report->RQ}}</td>
                           						@endif
                           						
                           						<td>{{$report->end}}</td>
                                                @if($product_group->group_name !== 'Others')
                           						    @foreach($reports1 as $report1)
													   @if($report1->prod_id == $report->product_id)
														@if(count($report1->qty_sold) > 0)
															<td>{{$report1->qty_sold}}</td>
															<td>P{{$report1->total_amount}}</td>
														@else
															<td>0</td>
															<td>P0</td>
														@endif
													   @endif
                           						    @endforeach
                                                @endif

                           					</tr>
                           					@endif
                           				@endforeach
                           			</tbody>
                           		</table>
                                
                                @if($product_group->group_name !== 'Others')
                                    <h4>SALES {{$product_group->group_name}}: &#8369;
                                        @if(count($product_group->SALES) > 0)
                                            {{$product_group->SALES}}
                                        @else
                                            0
                                        @endif
                                    </h4>
                                @endif
                           	</div>
                           	@endforeach

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    Sales:
                                    <div class="row">
                                        <ul>
                                            @foreach($product_groups as $pg)
                                                @if($pg->group_name !== 'Others')
                                                    @if(count($pg->SALES) > 0) 
                                                        <li><b>P{{$pg->SALES}}</b></li>
                                                    @else
                                                        <li>P0</li>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    Total: 
                                    @if(count($total_sales->total > 0))
                                        <b>P{{$total_sales->total}}</b>
                                    @else
                                        P0
                                    @endif
                                    <br>
                                    Senior Disc: 
                                    @if(count($total_discount > 0))
                                        <b>P{{$total_discount}}</b>
                                    @else
                                        P0
                                    @endif
                                    <br>
                                    Less expenses:
                                    <br>
                                    Sign cheat:
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    Net Sales:
                                    <br>
                                    Short:
                                    <br>
                                    Over:
                                    <br>
                                    Total Cash:
                                    <br>
                                    Delivery Sales:
                                </div>
                            </div>
                        </div> <!-- end div body -->

                    </div>
                </div>
            </div>
            
        </div>
    </section>

<script type="text/javascript">
    $('#print-reports-btn').click(function() {
        var current_time = new Date().toLocaleTimeString();
        //console.log(current_time);
        var timestamp = new Date().toJSON().slice(0,10);/*.replace(/-/g,'/')*/
        //console.log(timestamp);
        document.title = 'Emmans-Reports-'+timestamp+' '+current_time+'.pdf';
        window.print();
    });
</script>

@stop