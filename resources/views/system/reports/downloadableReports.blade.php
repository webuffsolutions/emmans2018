@extends('layouts.app-demo')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">print</i>
                        </div>
                        <div class="content">
                            <h3>DOWNLOAD REPORTS</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>List of downloadable reports</h2>

                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a onclick="addProductForm()">
                                                <i class="material-icons">group_add</i>
                                                Add Product
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul> -->

                        </div>

                        <div class="body" style="overflow-x:auto;overflow: visible;">
                            <table id="" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($downloadableReports as $report)
                                    <tr>
                                        <td>{{$report->id}}</td>
                                        <td>{{$report->name}}</td>
                                        <td>{{$report->created_at}}</td>
                                        <td>
                                            <a href="{{asset($report->pdf_file)}}" download 
                                                class="btn btn-primary waves-effect">Download</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            
        </div>
    </section>

@stop