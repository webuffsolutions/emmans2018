@extends('layouts.app-demo')

@section('content')

<section class="content">
	<div class="container-fluid">
		<!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-white">
                        <b>Filter by Category:</b><br><br>
							@foreach($product_groups as $product_group)
						  		<li style="display:inline-block;">
						  			<b>
							  			<a href="{{route('sell.filter', ['group_id' => $product_group->id])}}" class="btn btn-md bg-light-green waves-effect"> 
							  				{{$product_group->group_name}}
							  			</a>
						  			</b>
						  		</li>
						  	@endforeach
							
							<li style="display:inline-block;"><b>
						  		<form method="GET" action="{{route('sell.search')}}">
									<input type="hidden" name="all" value="{{ old('search') }}">
									<button class="btn btn-md bg-light-green waves-effect">Show All</button>
								</form>
						  	</li>
						
                    </div>
                    <div class="body">
                    	<div class="contaner">
							<form method="GET" action="{{route('sell.search')}}">
								<div class="row">
									<div class="col-md-6">
										<input type="text" name="product" class="form-control" placeholder="Search Items" value="{{ old('search') }}">
										<br>
										<button class="col-md-3 col-lg-5 col-sm-12 col-xs-12 btn bg-light-green waves-effect" style="opacity:0.8;">Search</button>
									</div>	
								</div>
							</form>
							@foreach($products as $product)
								<div class="col-lg-3 col-md-2 col-xs-2">
									<div class="contaner">
										<a href="{{ route('cart.add', ['id' => $product->id]) }}">
											<img src="{{ asset($product->image) }}" width="100px"; height="100px" alt="No Image">
										</a>
										<br>
										<small>{{$product->product_name}}
											<br>
											@if($product->in_stock <= 10)
												<font color="red">
													Stock: 
													{{$product->in_stock}}
												</font>
											@else
												Stock:
												{{$product->in_stock}}
											@endif
										</small>
										<br>					
											<!-- ADD MULTI QTY MODAL -->
											<!-- <a href="route('cart.add.multiple', ['id' => $product->id])}}" class="btn btn-xs bg-light-green waves-effect" style="opacity:0.8;">Add Multiple
											</a> -->

											<a onclick="addToCartMultipleForm({{$product->id}})" class="btn btn-xs bg-light-green waves-effect" style="opacity:0.8;">Add Multiple</a>

										<br><br><br>
										<input type="hidden" name="pdt_id" value="{{$product->id}}">
									</div>
								</div>
							@endforeach
							
							<div style="height:400px;"></div>
								<center>
									<div class="dataTables_info col-lg-12 col-md-12 col-sm-12 col-xs-12" id="example2_info" role="status" aria-live="polite">
										{{ $products->links() }}<br>
										Showing 1 to {{count($products)}} of {{count($products)}} entries
									</div>
								</center>
								<div style="height:150px;"></div>
        					</div>
                    	</div>
                	</div>
            	</div>

            	<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
					@include('includes.errors')
					<div class="card">
						<div class="header bg-light-green" style="opacity:0.8;">
							<b>Cart Items: {{ Cart::content()->count() }}</b><br>
						</div>

							<div class="panel-body" style="overflow-x:auto;overflow: visible;">
								<table id="data-table" class="table table-bordered table-striped table-hover">
									<thead>
										<tr>
											<th>Product</th>
											<th>Qty</th>
											<th>Total</th>
											<th width="50%">Action</th>
										</tr>
									</thead>
									<tbody>
											@foreach(Cart::content() as $cart_product)
											<tr>
												<td>{{ $cart_product->name }}</td>
												<td>
													{{ $cart_product->qty }}
												</td>
												<td width="30%">P{{ $cart_product->total() }}</td>
												<td width="40%">
													<a href="{{ route('cart.deduct', ['cart_row_id' => $cart_product->rowId, 'cart_product_id' => $cart_product->id, 
													'cart_product_qty' => $cart_product->qty ]) }}"
														class="btn btn-sm btn-danger waves-effect">-</a>

													<a href="{{ route('cart.remove', ['cart_row_id' => $cart_product->rowId, 'cart_product_id' => $cart_product->id, 
													'cart_product_qty' => $cart_product->qty ]) }}"
														class="btn btn-sm btn-danger waves-effect"><b>x</b></a>
												</td>
											</tr>
											@endforeach
									</tbody>
								</table>
							</div>
					</div>

					<div class="col-lg-12 col-md-6 col-sm-6 col-xs-6">
						<h3>Total: &#8369; {{ Cart::total() }}</h3><br>
						<form method="post">
						{{ csrf_field() }}
							
							<label for="scd">SCD :</label>
							<input type="number" name="scd" class="form-control" style="width:150px;" value="0"><br>
					</div>

					<div class="col-lg-12 col-md-6 col-sm-6 col-xs-6">
						<li style="display:inline-block;">
								<button formaction="{{ route('cart.checkout')}}" class="btn bg-light-green waves-effect" style="opacity:0.8;" 
								type="submit" onclick="return confirm('Are you sure you want to checkout?');">Checkout</button>

						        <button formaction="{{route('cart.empty')}}" class="btn bg-red waves-effect" style="opacity:0.8;" type="submit" 
						        onclick="return confirm('Are you sure you want to empty this cart?');">Empty Cart</button>
							</li>
						</form>
					</div>

				</div>
        	</div>
    	</div>
    </div>
</section>



@endsection