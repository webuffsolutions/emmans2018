@extends('layouts.app-demo')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">supervisor_account</i>
                        </div>
                        <div class="content">
                            <h3>PRODUCT GROUPS</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>List of product groups</h2>

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    @if(Auth::user()->role->id == App\User::IS_SUPERADMIN)
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a onclick="addProductGroupForm()">
                                                    <i class="material-icons">group_add</i>
                                                    Add Product Group
                                                </a>
                                            </li>
                                        </ul>
                                    @endif
                                </li>
                            </ul>

                        </div>

                        <div class="body" style="overflow-x:auto;">
                            <table id="product-groups-table" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>name</th>
                                        <th>date added</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            
        </div>
    </section>

@stop