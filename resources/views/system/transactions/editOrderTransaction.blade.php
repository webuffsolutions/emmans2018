@extends('layouts.app-demo')

@section('content')

<section class="content">
	<div class="container-fluid">
		<!-- Edit Transaction -->
		@if($orders->count() > 0)
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-light-green" style="opacity:0.8;">
                            <b>Transaction ID:</b> {{$orders[0]->transaction_id}} <b style="padding-left:500px">Date/Time:</b> {{ Carbon\Carbon::parse($orders[0]->created_at)->format('m/d/Y h:i:s A') }}
							<br>
							<b>Discount:</b> P{{$orders[0]->discount}}
							<br>
							<b>Cashier:</b> {{$orders[0]->cashier}}
							<br>
							<b style="">Authorized:</b> {{Auth::user()->name}}
                        </div>
                        <div class="body" style="overflow-x:auto;">
                            <table id="data-table" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Product</th>
										<th>Qty</th>
										<th>Amt</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Product</th>
										<th>Qty</th>
										<th>Amt</th>
										<th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                	@foreach($orders as $order)
									<tr>
										<td>{{$order->order_id}}</td>
										<td>{{$order->product_name}}</td>
										<td>{{$order->qty}}</td>
										<td>P{{$order->amount}}</td>
										<td>

											<button class="voidOrderQty-modal btn btn-sm bg-red"
											data-product_id="{{ $order->product_id}}" data-qty="{{ $order->qty}}" 
											data-order_id="{{$order->order_id}}">-</button>

			                            	<button class="voidOrder-modal btn btn-sm bg-red"
			                            		data-product_id="{{ $order->product_id }}" data-qty="{{$order->qty}}" 
			                            		data-order_id="{{$order->order_id}}">
			                            	VOID
			                            	</button>

										</td>
									</tr>
									@endforeach     
                                </tbody>
                            </table>
                            <div align="right" style="padding-right : 20px">
								<button class="voidTransaction-modal btn btn-sm bg-light-green" data-transaction_id="{{$order->transaction_id}}"> VOID TRANSACTION</button>
								<a href="{{ route('transactions.index') }}" class="btn btn-sm bg-light-green" style="opacity:0.8;">SAVE</a>&nbsp;
								<a href="{{ route('transactions.index') }}" class="btn btn-sm bg-light-green" style="opacity:0.8;">CANCEL</a>
								<br><br>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        @else
        	<!-- redirect user to transactions view page if $products is empty after voiding the last order-->
        	<script type="text/javascript">
		    	window.location = "{{route('transactions.index')}}"
			</script>
        @endif
	</div>
</section>

@endsection