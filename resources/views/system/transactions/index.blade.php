@extends('layouts.app-demo')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">layers</i>
                        </div>
                        <div class="content">
                            <h3>TRANSACTIONS</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Order Transactions</h2>

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">

                                        <li>
                                            <a href="
                                           		@if (Auth::user()->role->id == App\User::IS_ADMIN)
					                                {{ route('AdminDashboard') }}
					                            @elseif (Auth::user()->role->id == App\User::IS_CASHIER)
					                                {{ route('CashierDashboard') }}
					                            @endif
                                            ">
                                                Back to dashboard
                                            </a>
                                        </li>
                                       
                                    </ul>
                                </li>
                            </ul>

                        </div>

                        <div class="body" style="overflow-x:auto;">
                            <table id="" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>type</th>
                                        <th>user</th>
                                        <th>date added</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($order_transactions as $order_transaction)
                                	<tr>
                                		<td>{{$order_transaction->trans_id}}</td>
                                		<td>{{$order_transaction->type}}</td>
                                		<td>{{$order_transaction->user}}</td>
                                		<td>{{$order_transaction->created_at}}</td>
                                		<td>
                                			<a href="{{ route('transactions.edit', ['order_transactions_id' => $order_transaction->trans_id])}}" class='btn btn-md bg-light-green waves-effect'>Edit</a>
                                		</td>
                                	</tr>
                                	@endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Stock Transactions</h2>

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">

                                        <li>
                                            <a href="
                                                @if (Auth::user()->role->id == App\User::IS_ADMIN)
                                                    {{ route('AdminDashboard') }}
                                                @elseif (Auth::user()->role->id == App\User::IS_CASHIER)
                                                    {{ route('CashierDashboard') }}
                                                @endif
                                            ">
                                                Back to dashboard
                                            </a>
                                        </li>
                                       
                                    </ul>
                                </li>
                            </ul>

                        </div>

                        <div class="body" style="overflow-x:auto;">
                            <table id="" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>type</th>
                                        <th>user</th>
                                        <th>date added</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($stock_transactions as $stock_transaction)
                                    <tr>
                                        <td>{{$stock_transaction->trans_id}}</td>
                                        <td>{{$stock_transaction->type}}</td>
                                        <td>{{$stock_transaction->user}}</td>
                                        <td>{{$stock_transaction->created_at}}</td>
                                        <td>
                                            <a href="#" class="btn btn-md bg-light-green waves-effect">Edit</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            
        </div>
    </section>

@stop