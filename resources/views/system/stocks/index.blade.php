@extends('layouts.app-demo')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">layers</i>
                        </div>
                        <div class="content">
                            <h3>STOCKS MANAGEMENT</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>List of products</h2>
                            <span><font color="red">red</font> Indicates the critical level of stocks</span>

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">

                                        <li>
                                            <a href="
                                            @if(Auth::user()->role->id == App\User::IS_ADMIN)
                                                {{ route('AdminDashboard') }}
                                            @else
                                                {{ route('CashierDashboard') }}
                                            @endif
                                            ">
                                                Back to dashboard
                                            </a>
                                        </li>
                                       
                                    </ul>
                                </li>
                            </ul>

                        </div>

                        <div class="body" style="overflow-x:auto;">
                            <table id="stocks-table" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>name</th>
                                        <th>in stock</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            
        </div>
    </section>

@stop