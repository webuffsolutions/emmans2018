<!DOCTYPE html>
<html>
<head>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('upload/logo/webuff-logo.jpg') }}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Emman's Inventory System</title>

    @include('includes.css')
        <!-- MAIN JS FILES -->
    @include('includes.js')

</head>

<body class="theme-light-green">

    <!-- Page Loader -->

    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->

    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
   
    <!-- #END# Search Bar -->
    <!-- Nav Bar -->
    @include('includes.navbar')
    <!-- #Nav Bar -->
    <section>
    <!-- Left Sidebar -->
    @include('includes.sidebar')
    <!-- #END# Left Sidebar -->
    </section>
    
    <!-- MAIN CONTENT HERE -->
    @yield('content')
    <!-- -->

    <!-- MODALS -->
    @include('includes.modals.contacts-form') <!-- contacts -->
    @include('includes.modals.users-form') <!-- users -->
    @include('includes.modals.product-groups-form') <!-- product groups -->
    @include('includes.modals.products-form')<!-- products -->
    @include('includes.modals.stocks-form')<!-- stocks -->
    @include('includes.modals.addToCartMultiple-form')<!-- add multiple items to cart -->
    @include('includes.modals.voidOrder-form')<!-- void order form -->
    @include('includes.modals.voidOrderQty-form')<!-- void order qty form -->
    @include('includes.modals.voidTransaction-form')<!-- void transaction form -->
    <!-- / -->
    
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <!-- / -->
    @include('includes.custom-js.contacts-js')<!--CONTACTS JS FILES-->
    @include('includes.custom-js.users-js')<!--USERS JS FILES-->
    @include('includes.custom-js.product-groups-js')<!--PRODUCT GROUPS JS FILES-->
    @include('includes.custom-js.products-js')<!-- PRODUCTS JS FILES-->
    @include('includes.custom-js.stocks-js')<!-- STOCKS JS FILES-->
    @include('includes.custom-js.addToCartMultiple-js')<!-- ADD MULTIPLE ITEM TO CART JS FILES-->
    @include('includes.custom-js.voidOrder-js')<!-- Void order form js-->
    @include('includes.custom-js.voidOrderQty-js')<!-- Void order qty form js-->
    @include('includes.custom-js.voidTransaction-js')<!-- Void transaction form js-->

</body>

</html>