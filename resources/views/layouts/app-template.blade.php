<!DOCTYPE html>
<html>
<head>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('upload/logo/webuff-logo.jpg') }}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Emman's Inventory System</title>
    
    <!-- CSS HERE -->
    @include('includes.css')
    <!-- -->

</head>

<body class="theme-light-green">

    <!-- Page Loader -->
    <!-- <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div> -->
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->


    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
   
    <!-- #END# Search Bar -->
    <!-- Nav Bar -->
    @include('includes.navbar')
    <!-- #Nav Bar -->
    <section>
    <!-- Left Sidebar -->
    @include('includes.sidebar')
    <!-- #END# Left Sidebar -->
    </section>

    <!-- JS HERE -->
    @include('includes.js')
    <!-- -->
    
    <!-- MAIN CONTENT HERE -->
    @yield('content')
    <!-- -->

    
</body>

</html>