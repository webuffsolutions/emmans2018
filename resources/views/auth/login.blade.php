<!DOCTYPE html>
<html lang="en">
<head>
<title>Emman's Inventory System</title>
<!-- Meta tag Keywords -->
<link rel="icon" href="{{ asset('upload/logo/webuff-logo.jpg') }}" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->
<!-- css files -->
<link rel="stylesheet" href="{{ asset('login-form/css/font-awesome.css') }}"> <!-- Font-Awesome-Icons-CSS -->
<link rel="stylesheet" href="{{ asset('login-form/css/style.css') }}" type="text/css" media="all" /> <!-- Style-CSS --> 
<link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
<!-- //css files -->
<!-- web-fonts -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700" rel="stylesheet">
<!-- //web-fonts -->
</head>
<body>
        <!--header-->
        <div class="header-w3l">
            <h1>EMMAN'S INVENTORY SYSTEM</h1>
        </div>
        <!--//header-->
        <!--main-->
        <div class="main-w3layouts-agileinfo">
               <!--form-stars-here-->
                        <div class="wthree-form">
                            <h2>Fill out the form below to login</h2>
                            <form method="POST" action="{{ route('AuthLogin') }}">
                                {{ csrf_field() }}

                                <div class="form-sub-w3{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <input type="text" name="username" id="username" placeholder="Username " 
                                    value="{{ old('username') }}" required />

                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif

                                <div class="icon-w3">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </div>
                                </div>
                                <div class="form-sub-w3{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input type="password" id="password" name="password" placeholder="Password" required />

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif

                                <div class="icon-w3">
                                    <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                </div>
                                </div>
                                <!-- <label class="anim">
                                <input type="checkbox" class="checkbox">
                                    <span>Remember Me</span> 
                                    <a href="#">Forgot Password</a>
                                </label>  -->
                                <div class="clear"></div>
                                <div class="submit-agileits">
                                    <input type="submit" value="Login">
                                </div>
                            </form>

                        </div>
                <!--//form-ends-here-->

        </div>
        <!--//main-->
        <!--footer-->
        <div class="footer">
            <p>&copy; 2018 
                Emman's Inventory System. All rights reserved <br>
                Powered by <a href="http://www.webuffsolutions.com">Webuff Solutions.</a>
            </p>
        </div>
        <!--//footer-->

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/toastr.min.js') }}"></script>

        <script>
            @if(Session::has('success'))
                toastr.success("{{ Session::get('success') }}")
            @endif
            @if(Session::has('info'))
                toastr.info("{{ Session::get('info') }}")
            @endif
            @if(Session::has('error'))
                toastr.error("{{ Session::get('error') }}")
            @endif
        </script>
</body>
</html>