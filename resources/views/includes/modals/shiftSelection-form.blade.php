<div class="modal" id="shiftSelection-modal-form" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
            </div>
            <form id="form-shiftSelection" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}
                
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="shift">Shift</label>
                        <div class="col-sm-6">
                            <!--<input type="text" name="role_id" id="role_id1">-->
                            <div class="form-line">
                                <select class="form-control" id="shift" name="shift" required>
                                    <option value="">- Select Shift -</option>
                                    <option value="AM">AM</option>
                                    <option value="PM">PM</option>
                                </select>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                    <br>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="shift_option">Option</label>
                        <div class="col-sm-6">
                            <!--<input type="text" name="role_id" id="role_id1">-->
                            <div class="form-line">
                                <select class="form-control" id="shift_option" name="shift_option" required>
                                    <option value="">- Select Option -</option>
                                    <option value="new">New shift</option>
                                    <option value="resume">Resume Previous Shift</option>
                                </select>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <center>
                        <br>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="loaderMessage"></div><br>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="loaderimage">
                        </div>
                    </center>
                   
                </div>

                <div class="modal-footer">
                    <button type="submit" id="shiftSelButton" class="btn btn-primary waves-effect shiftSelection">Submit</button>
                </div>

            </form>
        </div>
    </div>
</div>
