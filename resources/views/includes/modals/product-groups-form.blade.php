<div class="modal" id="product-groups-modal-form" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel"></h4>
            </div>
            <form id="form-product-groups" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="group_name">Group Name:</label>
                        <div class="col-sm-6">
                            <div class="form-line">
                                <input type="text" id="group_name" name="group_name" class="form-control" required>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">Submit</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                </div>

            </form>
        </div>
    </div>
</div>
