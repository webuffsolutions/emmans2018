<div class="modal" id="addMultipleToCart-modal-form" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel"></h4>
            </div>
            <form id="form-addMultipleToCart" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-body">
                    <input type="hidden" id="id2" name="id">
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="product_name">Product:</label>
                        <div class="col-sm-6">
                            <div class="">
                                <input type="text" id="product_name2" name="" class="form-control" disabled>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="in_stock">Current Stock:</label>
                        <div class="col-sm-6">
                            <div class="">
                                <input type="number" id="in_stock2" name="" class="form-control" disabled>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="qty">Qty:</label>
                        <div class="col-sm-6">
                            <div class="form-line">
                                <input type="number" min="1" id="qty" name="qty" class="form-control" required>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">Add To Cart</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                </div>

            </form>
        </div>
    </div>
</div>
