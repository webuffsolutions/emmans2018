<div class="modal" id="voidTransaction-modal-form" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel"></h4>
            </div>
            <form id="form-voidTransaction" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-body">
                    <input type="hidden" id="transaction_id" name="transaction_id">
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="reason">Reason</label>
                        <div class="col-sm-12">
                            <div class="form-line">
                                <select class="form-control" id="reason2" name="reason" required>
                                        <option value="">- Select Reason -</option>
                                        <option value="customer request">Customer Request</option>
                                        <option value="human error">Human Error</option>
                                </select>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect voidTransaction">Submit</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                </div>

            </form>
        </div>
    </div>
</div>
