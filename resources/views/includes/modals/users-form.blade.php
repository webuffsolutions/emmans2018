<div class="modal" id="users-modal-form" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel"></h4>
            </div>
            <form id="form-users" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="name">Name:</label>
                        <div class="col-sm-6">
                            <div class="form-line">
                                <input type="text" id="name1" name="name" class="form-control" required>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="username">Username:</label>
                        <div class="col-sm-6">
                            <div class="form-line">
                                <input type="text" id="username" name="username" class="form-control" autofocus required>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    @php
                        $roles = App\Role::all();
                    @endphp

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="role_id">Role</label>
                        <div class="col-sm-6">
                            <!--<input type="text" name="role_id" id="role_id1">-->
                            <div class="form-line">
                                <select class="form-control" id="role_id" name="role_id" required>
                                        <option value="">- Select Role -</option>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}">{{$role->role_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">Submit</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                </div>

            </form>
        </div>
    </div>
</div>


