<div class="modal" id="products-modal-form" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
            </div>
            <form id="form-products" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}
                
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="product_name">Product Name:</label>
                        <div class="col-sm-6">
                            <div class="form-line">
                                <input type="text" id="product_name" name="product_name" class="form-control" autofocus required>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    @php
                        $product_groups = App\ProductGroup::all();
                    @endphp

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="role_id">Product Group</label>
                        <div class="col-sm-6">
                            <!--<input type="text" name="role_id" id="role_id1">-->
                            <div class="form-line">
                                <select class="form-control" id="group_id" name="group_id" required>
                                        <option value="">- Select Group -</option>
                                        @foreach($product_groups as $product_group)
                                            <option value="{{$product_group->id}}">{{$product_group->group_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="price">Price:</label>
                        <div class="col-sm-6">
                            <div class="form-line">
                                <input type="number" id="price" name="price" min="1" class="form-control" autofocus required>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="image">Image:</label>
                        <div class="col-sm-6">
                            <div class="">
                                <input type="file" id="image" name="image" class="form-control" required>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">Submit</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                </div>

            </form>
        </div>
    </div>
</div>
