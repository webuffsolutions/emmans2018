<div class="modal" id="confirmLogout-modal-form" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel"></h4>
            </div>
            <form id="form-confirmLogout" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}
                
                <div class="modal-body">
                    <div class="col-lg-12">
                        <label for="confirm_message">Are you sure you want to logout ?</label>

                        <label style="color:red;">*Make sure to settle the following before you logout:</label>
                        <br>
                        <i class="material-icons" style="position:relative;top:5px;left:5px;margin-right:7px;">print</i><a href="{{route('reports.view')}}">Print reports</a><br>

                        <i class="material-icons" style="position:relative;top:5px;left:5px;margin-right:7px;">library_add</i><a href="{{route('stocks.index')}}">Stocks Management</a><br>

                        <i class="material-icons" style="position:relative;top:5px;left:5px;margin-right:7px;">shopping_cart</i><a href="{{route('sell')}}">Check Shopping Cart</a>
                        
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect confirmLogoutBtn">Confirm Logout</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                </div>

            </form>
        </div>
    </div>
</div>
