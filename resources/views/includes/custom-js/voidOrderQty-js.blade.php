<!-- VOID MULTIPLE ORDER QTY -->
<script type="text/javascript">
	$(document).on('click', '.voidOrderQty-modal', function() {

		var a = $(this).data('product_id');
		var b = $(this).data('qty');
		var c = $(this).data('order_id');

		/*console.log('product id: '+a+'\n'+'qty: '+b+'\n'+'order id: '+c+'\n');*/

		$('#product_id2').val($(this).data('product_id'));
		$('#qty2').val($(this).data('qty'));
		$('#order_id2').val($(this).data('order_id'));

		product_id = $('#product_id2').val();
		qty = $('#qty2').val();
		order_id = $('#order_id2').val();

		$('.modal-title').text('Void Order Quantity');
		$('#voidOrderQty-modal-form').modal('show');
	});
	$('#voidOrderQty-modal-form form').validator().on('submit', function (e) {
		if (!e.isDefaultPrevented()){
			url = "{{ url('void') . '/' }}" + 'order' + '/' + 'qty' + '/' + product_id + '/' + qty + '/' + order_id;

			$.ajax({
				url: url,
				type: 'POST',
				data: {
					'void_qty' : $("#void_qty").val(),
					'reason': $("#reason1").val(),
				},
				success: function(data) {
					$('#voidOrderQty-modal-form').modal('hide');
					$('#voidOrderQty-modal-form form')[0].reset();
					if(data.error == 'invalidVoidOrderQty'){
                        swal({
                            title: 'Error!',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        });
                    } else {
                    	$('#voidOrderQty-modal-form form')[0].reset();
                    	swal({
                        	title: 'Success!',
                        	text: data.message,
                        	type: 'success',
                        	timer: '1500'
                    	})
                    	window.location.reload();
                    }
				},
				error: function() {
					swal({
                        title: 'Something went wrong! Please refresh the page.',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    }) //end swal
				}
			});

			return false;
		}
	});
</script>