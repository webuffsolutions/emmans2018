<!-- FOR ADD MULTIPLE ITEMS TO CART CRUD -->
<script type="text/javascript">

    function addToCartMultipleForm(id)
    {
        console.log(id);
        save_method = 'addMulti';
        $('input[name=_method]').val('POST');
        $('#addMultipleToCart-modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('cart') }}" + '/' + 'add' + '/' + 'multi' + '/' + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) { //data = json id from url..
                console.log(data.id);
                $('#addMultipleToCart-modal-form').modal('show');
                $('.modal-title').text('Add Multiple Qty');
                $('#id2').val(data.id);
                $('#product_name2').val(data.product_name);
                $('#in_stock2').val(data.in_stock);
            },
            error : function() {
                alert('no data');
            }
        });

    }

    $(function(){
        $('#addMultipleToCart-modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id2').val();
                if (save_method == 'addMulti') url = "{{ url('cart') }}" + '/' + 'store' + '/' + 'multi' + '/' + id;

                $.ajax({
                    url : url,
                    type : "POST",
                    data: new FormData($("#addMultipleToCart-modal-form form")[0]),
                    contentType: false,
                    processData: false,
                    success : function(data) {
                        $('#addMultipleToCart-modal-form').modal('hide');
                        console.log(data);
                        if (data.error == 'notEnoughStocks'){
                            swal({
                                title: 'Error!',
                                text: data.message,
                                type: 'error',
                                timer: '1500'
                            });
                        } else {
                            window.location.reload();
                        }
                        /*else {
                            swal({
                                title: 'Success!',
                                text: data.message,
                                type: 'success',
                                timer: '1500'
                            })
                        }*/
                    }, //end success
                    error : function(data){
                        console.log(data);
                        $('#addMultipleToCart-modal-form').modal('hide');
                        //table.ajax.reload();
                        swal({
                            title: 'Please try again.',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        }) //end swal
                        /*window.location.reload();*/
                    } //end error
                });
                return false;
            }
        });
    });
</script>