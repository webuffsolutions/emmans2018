<script type="text/javascript">
	/*$(document).on('click', '.confirmLogout-modal', function() {
		alert('logout...');
	});*/
	function confirmLogout() {
		console.log('logout!');

		$('.modal-title').text('Logout Confirmation');
		$('#confirmLogout-modal-form').modal('show');

		$('#confirmLogout-modal-form form').validator().on('submit', function (e) {
			if (!e.isDefaultPrevented()){
				url = "{{ url('user') . '/' }}" + 'logout12';

				$.ajax({
					url: url,
					type: 'GET',
					success: function() {
						$('#confirmLogout-modal-form').modal('hide');
                		
					},
					error: function() {
	                    $('#confirmLogout-modal-form').modal('hide');
                		window.location.href = "{{URL::to('user/logout')}}"
					}
				});

				return false;
			}
		});
	}
</script>