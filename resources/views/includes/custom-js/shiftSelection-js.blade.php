<script type="text/javascript">
    $(window).on('load',function(){
        $('#shiftSelection-modal-form').modal('show');
    });
    $('#shiftSelection-modal-form').modal({backdrop: 'static', keyboard: false});
    $('.modal-title').text('Shift Selection');

    $('#shiftSelection-modal-form form').validator().on('submit', function (e) {

    	if (!e.isDefaultPrevented()){
    		url = "{{ url('shift') . '/' }}" + 'selection';
    		$('#shiftSelButton').attr("disabled", true);
            //alert('please wait while we are processing your shift');
            var div = document.getElementById('loaderMessage');
            div.innerHTML += '<font color="red">Please wait while we are processing your shift.</font>';

            var div = document.getElementById('loaderimage');
            div.innerHTML += '<img id="loader-img" src="{{ asset('upload/logo/loader.gif') }}" width="100px;" />';

    		$.ajax({
				url: url,
				type: 'GET',
				data: {
					'shift' : $("#shift").val(),
					'shift_option': $("#shift_option").val(),
				},
				success: function(data) {
					$('#shiftSelection-modal-form').modal('hide');
					$('#shiftSelection-modal-form form')[0].reset();
					if(data.error == 'invalidVoidOrderQty'){
                        swal({
                            title: 'Error!',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        });
                    } else {
                    	$('#shiftSelection-modal-form form')[0].reset();
                    	swal({
                        	title: 'Success!',
                        	text: data.message,
                        	type: 'success',
                        	timer: '1500'
                    	})
                    	window.location.href = "{{URL::to('/cashier/dashboard')}}"
                    }
                },
				error: function() {
					swal({
                        title: 'Something went wrong! Please refresh the page.',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    }) //end swal
				}
			});

			return false;
    	}
    });
</script>