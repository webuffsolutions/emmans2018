<!-- FOR USERS CRUD -->
<script type="text/javascript">
    var productGroupsTable = $('#product-groups-table').DataTable({
        processing: false,
        serverSide: false,
        ajax: "{{ route('api.productgroups') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'group_name', name: 'name'},
            {data: 'created_at', name: 'date added'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

    function addProductGroupForm() {
        //console.log('add users form!');
        save_method = "add";
        //var x = document.getElementById("role_id1").required = true;
        $('input[name=_method]').val('POST');
        $('#product-groups-modal-form').modal('show');
        $('#product-groups-modal-form form')[0].reset();
        $('.modal-title').text('Add Product Group');
    }

    function editProductGroupForm(id) {
        console.log('ProductGroup = '+ id);
        save_method = 'edit';
        $('input[name=_method]').val('PATCH');
        $('#product-groups-modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('productgroups') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON", 
            success: function(data) { //data = json id from url..
                console.log(data);
                $('#product-groups-modal-form').modal('show');
                $('.modal-title').text('Edit Product Group');
                $('#id').val(data.id);
                $('#group_name').val(data.group_name); 
            },
            error : function() {
                alert("No Data");
            }
        });
    }

    function deleteProductGroupData(id) {
        console.log('deleting..');
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        })//end swal
        .then(function () {
            $.ajax({
                url : "{{ url('productgroups') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    productGroupsTable.ajax.reload();
                    swal({
                        title: 'Success!',
                        text: data.message,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function () {
                    swal({
                        title: 'Something went wrong! Please refresh the page.',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    }) //end swal
                } //end error
            }); //end $.ajax
        });//end .then()
    }//end deleteProductGroupData()

    //STORE AND UPDATE
    $(function(){
        $('#product-groups-modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('productgroups') }}";
                else url = "{{ url('productgroups') . '/' }}" + id; //for update

                $.ajax({
                    url : url,
                    type : "POST",
                    data: new FormData($("#product-groups-modal-form form")[0]),
                    contentType: false,
                    processData: false,
                    success : function(data) {
                        $('#product-groups-modal-form').modal('hide');
                        productGroupsTable.ajax.reload();
                        console.log(data);
                        if(data.error == 'productGroupExists'){
                            swal({
                                title: 'Error!',
                                text: data.message,
                                type: 'error',
                                timer: '1500'
                            });
                        } else if(data.error == 'productGroupExsistsUpdate') {
                            swal({
                                title: 'Error!',
                                text: data.message,
                                type: 'error',
                                timer: '1500'
                            });
                        } else {
                            swal({
                                title: 'Success!',
                                text: data.message,
                                type: 'success',
                                timer: '1500'
                            })
                        }
                    }, //end success
                    error : function(data){
                        console.log(data);
                        $('#product-groups-modal-form').modal('hide');
                        //table.ajax.reload();
                        swal({
                            title: 'Please try again.',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        }) //end swal
                        //window.location.reload();
                    } //end error
                });
                return false;
            }
        });
    });

</script>