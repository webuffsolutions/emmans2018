<!-- FOR USERS CRUD -->
<script type="text/javascript">
    var usersTable = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('api.users') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'role', name: 'role'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

    function addUsersForm() {
        //console.log('add users form!');
        save_method = "add";
        //var x = document.getElementById("role_id1").required = true;
        $('input[name=_method]').val('POST');
        $('#users-modal-form').modal('show');
        $('#users-modal-form form')[0].reset();
        $('.modal-title').text('Add User');
    }

    function editUserForm(id) {
        console.log('UserID = '+ id);
        save_method = 'edit';
        $('input[name=_method]').val('PATCH');
        $('#users-modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('users') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON", 
            success: function(data) { //data = json id from url..
                console.log(data);
                $('#users-modal-form').modal('show');
                $('.modal-title').text('Edit User');
                $('#id').val(data.id);
                $('#name1').val(data.name);
                $('#username').val(data.username);
                $("#role_id").val(data.role_id).attr({"selected": true});   
            },
            error : function() {
                alert("No Data");
            }
        });
    }

    function deleteUserData(id) {
        //console.log('delete user data');
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        })//end swal
        .then(function () {
            $.ajax({
                url : "{{ url('users') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    usersTable.ajax.reload();
                    swal({
                        title: 'Success!',
                        text: data.message,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function () {
                    swal({
                        title: 'Something went wrong! Please refresh the page.',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    }) //end swal
                } //end error
            }); //end $.ajax
        });//end .then()
    }//end deleteUserData()

    $(function(){
        $('#users-modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('users') }}";
                else url = "{{ url('users') . '/' }}" + id; //for update

                $.ajax({
                    url : url,
                    type : "POST",
                    data: new FormData($("#users-modal-form form")[0]),
                    contentType: false,
                    processData: false,
                    success : function(data) {
                        $('#users-modal-form').modal('hide');
                        usersTable.ajax.reload();
                        console.log(data);
                        if(data.error == 'userExists'){
                            swal({
                                title: 'Error!',
                                text: data.message,
                                type: 'error',
                                timer: '1500'
                            });
                        } else {
                            swal({
                                title: 'Success!',
                                text: data.message,
                                type: 'success',
                                timer: '1500'
                            })
                        }
                    }, //end success
                    error : function(data){
                        console.log(data);
                        $('#users-modal-form').modal('hide');
                        //table.ajax.reload();
                        swal({
                            title: 'Please try again.',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        }) //end swal
                        //window.location.reload();
                    } //end error
                });
                return false;
            }
        });
    });
</script>