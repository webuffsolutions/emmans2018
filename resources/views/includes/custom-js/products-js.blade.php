<!-- FOR CONTACTS CRUD -->
<script type="text/javascript">
    var productsTable = $('#products-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('api.products') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'group_name', name: 'group'},
            {data: 'image', name: 'image', orderable: false, searchable: false},
            {data: 'product_name', name: 'name'},
            {data: 'price', name: 'price'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

    function addProductForm() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#products-modal-form').modal('show');
        $('#products-modal-form form')[0].reset();
        $('.modal-title').text('Add Product');
    }

    function editProductForm(id) {
        console.log('contact id: '+id);
        save_method = 'edit';
        $('input[name=_method]').val('PATCH');
        $('#products-modal-form form')[0].reset();
        var y = document.getElementById("image").required = false;
        $.ajax({
            url: "{{ url('products') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON", 
            success: function(data) { //data = json id from url..
                console.log(data);
                //to access other data.. ex: data.user.name
                $('#products-modal-form').modal('show');
                $('.modal-title').text('Edit Product');
                $('#id').val(data.product.id);
                $('#product_name').val(data.product.product_name);
                $("#group_id").val(data.product.group_id).attr({"selected": true});
                $("#price").val(data.product.price); 

            },
            error : function() {
                alert("No Data");
            }
        });
    }

    function deleteProductData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        }) //end swal
        .then(function () {
            $.ajax({
                url : "{{ url('products') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    productsTable.ajax.reload();
                    swal({
                        title: 'Success!',
                        text: data.message,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function () {
                    swal({
                        title: 'Something went wrong! Please refresh the page.',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    }) //end swal
                } //end error
            }); //end $.ajax
        }); //end .then
    }

    //STORE AND UPDATE
    $(function(){
        $('#products-modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('products') }}";
                else url = "{{ url('products') . '/' }}" + id; //for update

                $.ajax({
                    url : url,
                    type : "POST",
                    //data : $('#modal-form form').serialize(),
                    data: new FormData($("#products-modal-form form")[0]),
                    contentType: false,
                    processData: false,
                    success : function(data) {
                        $('#products-modal-form').modal('hide');
                        productsTable.ajax.reload();
                        console.log(data);
                        if(data.error == 'productExists'){
                            swal({
                                title: 'Error!',
                                text: data.message,
                                type: 'error',
                                timer: '1500'
                            });
                        } else if (data.error == 'productExsistsUpdate'){
                            swal({
                                title: 'Error!',
                                text: data.message,
                                type: 'error',
                                timer: '1500'
                            });
                        }
                        else {
                            swal({
                                title: 'Success!',
                                text: data.message,
                                type: 'success',
                                timer: '1500'
                            })
                        }
                    }, //end success
                    error : function(data){
                        $('#products-modal-form').modal('hide');
                        //table.ajax.reload();
                        swal({
                            title: 'Please try again.',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        }) //end swal
                        //window.location.reload();
                    } //end error
                });
                return false;
            }
        });
    });
</script>