<!-- VOID TRANSACTION -->
<script type="text/javascript">
	$(document).on('click', '.voidTransaction-modal', function() {
		var a = $(this).data('transaction_id');

		$('#transaction_id').val($(this).data('transaction_id'));
		transaction_id = $('#transaction_id').val();

		$('.modal-title').text('Void Transaction');
		$('#voidTransaction-modal-form').modal('show');
	});
	$('#voidTransaction-modal-form form').validator().on('submit', function (e) {
		console.log('voiding trans.');
		if (!e.isDefaultPrevented()){
			url = "{{ url('void') . '/' }}" + 'transaction' + '/' + transaction_id ;

			$.ajax({
				url: url,
				type: 'POST',
				data: {
					'transaction_id' : $("#transaction_id").val(),
					'reason': $("#reason2").val(),
				},
				success: function(data) {
					$('#voidTransaction-modal-form').modal('hide');
					$('#voidTransaction-modal-form form')[0].reset();
                	swal({
                    	title: 'Success!',
                    	text: data.message,
                    	type: 'success',
                    	timer: '1500'
                	})
                    window.location.reload();
				},
				error: function() {
					swal({
                        title: 'Something went wrong! Please refresh the page.',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    }) //end swal
				}
			});

			return false;
		}
	});
</script>