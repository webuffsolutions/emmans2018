<!-- VOID ORDER FORM -->
<script type="text/javascript">
	$(document).on('click', '.voidOrder-modal', function() {
		var a = $(this).data('product_id');
		var b = $(this).data('qty');
		var c = $(this).data('order_id');

		/*console.log('product id: '+a+'\n'+'quantity: '+b+'\n'+'order id: '+c);*/

		$('#product_id').val($(this).data('product_id'));
		$('#qty1').val($(this).data('qty'));
		$('#order_id').val($(this).data('order_id'));

		product_id = $('#product_id').val();
		qty = $('#qty1').val();
		order_id = $('#order_id').val();

		$('.modal-title').text('Void Order');
		$('#voidOrder-modal-form').modal('show');
	});
	$('#voidOrder-modal-form form').validator().on('submit', function (e) {
		if (!e.isDefaultPrevented()){
			url = "{{ url('void') . '/' }}" + 'order' + '/' + product_id + '/' + qty + '/' + order_id; 

			$.ajax({
				url: url,
				type: 'GET',
				data: {
					'product_id' : $("#product_id").val(),
					'qty' : $("#qty1").val(),
					'order_id' : $("#order_id").val(),
					'reason' : $("#reason").val(),
				},
				success : function(data) {
					$('#voidOrder-modal-form').modal('hide');
					/*console.log(data);*/
					window.location.reload();
					if(data.error == 'voidError'){
                        swal({
                            title: 'Error!',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        });
                    } else {
                    	swal({
                        	title: 'Success!',
                        	text: data.message,
                        	type: 'success',
                        	timer: '1500'
                    	})
                    	
                    }
                },
                error : function () {
                    swal({
                        title: 'Something went wrong! Please refresh the page.',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    }) //end swal
                } //end error
			});
			return false;
		}
	});
</script>