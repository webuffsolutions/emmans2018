<!-- FOR STOCKS ADD/LESS -->
<script type="text/javascript">
    var stocksTable = $('#stocks-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('api.stocks') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'product_name', name: 'product name'},
            {data: 'in_stock', name: 'in stock'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

    function addStockForm(id) {
        console.log('(stocks) product id: '+id);
        save_method = 'add_stock';
        $('input[name=_method]').val('POST');
        $('#stocks-modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('stocks') }}" + '/add/' + id,
            type: "GET",
            dataType: "JSON", 
            success: function(data) { //data = json id from url..
                console.log(data);
                //to access other data.. ex: data.user.name
                $('#stocks-modal-form').modal('show');
                $('.modal-title').text('Add Stocks');
                $('#id1').val(data.id);
                $('#product_name1').val(data.product_name);
                $('#in_stock').val(data.in_stock);
            },
            error : function() {
                alert("No Data");
            }
        });
    }

    function lessStockForm(id) {
        console.log('(less stocks) product id: '+id);
        save_method = 'less_stock';
        $('input[name=_method]').val('POST');
        $('#stocks-modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('stocks') }}" + '/less/' + id,
            type: "GET",
            dataType: "JSON", 
            success: function(data) { //data = json id from url..
                console.log(data);
                //to access other data.. ex: data.user.name
                $('#stocks-modal-form').modal('show');
                $('.modal-title').text('Less Stocks');
                $('#id1').val(data.id);
                $('#product_name1').val(data.product_name);
                $('#in_stock').val(data.in_stock);
            },
            error : function() {
                alert("No Data");
            }
        });
    }

    //STORE AND UPDATE
    $(function(){
        $('#stocks-modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id1').val();
                if (save_method == 'add_stock') url = "{{ url('stocks') . '/' . 'addStore' . '/' }}" + id;
                else url = "{{ url('stocks') . '/' . 'lessStore' . '/' }}" + id; //for update

                $.ajax({
                    url : url,
                    type : "POST",
                    data: new FormData($("#stocks-modal-form form")[0]),
                    contentType: false,
                    processData: false,
                    success : function(data) {
                        $('#stocks-modal-form').modal('hide');
                        stocksTable.ajax.reload();
                        console.log(data);
                        if (data.error == 'greaterThanInstock'){
                            swal({
                                title: 'Error!',
                                text: data.message,
                                type: 'error',
                                timer: '1500'
                            });
                        }
                        else {
                            swal({
                                title: 'Success!',
                                text: data.message,
                                type: 'success',
                                timer: '1500'
                            })
                        }
                        
                    }, //end success
                    error : function(data){
                        $('#stocks-modal-form').modal('hide');
                        //table.ajax.reload();
                        swal({
                            title: 'Please try again.',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        }) //end swal
                        //window.location.reload();
                    } //end error
                });
                return false;
            }
        });
    });
</script>