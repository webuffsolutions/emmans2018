<!-- FOR CONTACTS CRUD -->
<script type="text/javascript">
    var contactsTable = $('#contact-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('api.contact') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'show_photo', name: 'show_photo', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

    function addForm() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();
        $('.modal-title').text('Add Contact');
    }

    function editForm(id) {
        console.log('contact id: '+id);
        save_method = 'edit';
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('contact') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON", 
            success: function(data) { //data = json id from url..
                console.log(data);
                $('#modal-form').modal('show');
                $('.modal-title').text('Edit Contact');
                $('#id').val(data.id);
                $('#name').val(data.name);
                $('#email').val(data.email);
            },
            error : function() {
                alert("No Data");
            }
        });
    }

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        }) //end swal
        .then(function () {
            $.ajax({
                url : "{{ url('contact') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    contactsTable.ajax.reload();
                    swal({
                        title: 'Success!',
                        text: data.message,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function () {
                    swal({
                        title: 'Something went wrong! Please refresh the page.',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    }) //end swal
                } //end error
            }); //end $.ajax
        }); //end .then
    }

    $(function(){
        $('#modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('contact') }}";
                else url = "{{ url('contact') . '/' }}" + id; //for update

                $.ajax({
                    url : url,
                    type : "POST",
                    //data : $('#modal-form form').serialize(),
                    data: new FormData($("#modal-form form")[0]),
                    contentType: false,
                    processData: false,
                    success : function(data) {
                        $('#modal-form').modal('hide');
                        contactsTable.ajax.reload();
                        console.log(data);
                        if(data.error == 'contactExists'){
                            swal({
                                title: 'Error!',
                                text: data.message,
                                type: 'error',
                                timer: '1500'
                            });
                        } else {
                            swal({
                                title: 'Success!',
                                text: data.message,
                                type: 'success',
                                timer: '1500'
                            })
                        }
                    }, //end success
                    error : function(data){
                        $('#modal-form').modal('hide');
                        //table.ajax.reload();
                        swal({
                            title: 'Please try again.',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        }) //end swal
                        window.location.reload();
                    } //end error
                });
                return false;
            }
        });
    });
</script>