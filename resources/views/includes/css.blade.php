   <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Google Fonts -->
    <!-- custom made css including woff2 files -->
    <link href="{{ asset('material_template/css/googleapis/googleapis.min.css') }}" rel="stylesheet" media="all"/>
    
    <!-- Bootstrap Core Css -->
    <link href="{{ asset('material_template/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet" media="all">

    <!-- Waves Effect Css -->
    <link href="{{ asset('material_template/plugins/node-waves/waves.css') }}" rel="stylesheet" media="all"/>

    <!-- Animation Css -->
    <link href="{{ asset('material_template/plugins/animate-css/animate.css') }}" rel="stylesheet" media="all"/>

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('material_template/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" media="all"/>

        <!-- Wait Me Css -->
    <link href="{{ asset('material_template/plugins/waitme/waitMe.css') }}" rel="stylesheet" media="all"/>


    <!-- Bootstrap Select Css -->
    <link href="{{ asset('material_template/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" media="all"/>

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('material_template/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" 
    rel="stylesheet" media="all">

    <script src="{{ asset('sweetalert2/sweetalert2.min.js') }}"></script>
    <link href="{{ asset('sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" media="all">

    <!-- Morris Chart Css-->
    <link href="{{ asset('material_template/plugins/morrisjs/morris.css') }}" rel="stylesheet" media="all"/>

    <!-- Custom Css -->
    <link href="{{ asset('material_template/css/style.css') }}" rel="stylesheet" media="all">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('material_template/css/themes/all-themes.css') }}" rel="stylesheet" media="all"/>
    
    <!-- Toastr notification pop-up -->
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}" media="all">


