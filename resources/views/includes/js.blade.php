    

    <!-- Jquery Core Js -->
    <script src="{{ asset('material_template/plugins/jquery/jquery.min.js') }}"></script>
    <!-- <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script> -->
    
    <script src="{{ asset('material_template/plugins/bootstrap/js/bootstrap.js') }}"></script>

     <!-- Moment Plugin Js -->
    <script src="{{ asset('material_template/plugins/momentjs/moment.js') }}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('material_template/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <!-- Select Plugin Js -->
    <!-- <script src=" asset('material_template/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script> -->

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('material_template/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('material_template/plugins/node-waves/waves.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('material_template/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('material_template/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>

    <!-- Validator -->
    <script src="{{ asset('validator/validator.min.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('material_template/js/admin.js') }}"></script>
    <script src="{{ asset('material_template/js/pages/ui/modals.js') }}"></script>

    <script src="{{ asset('material_template/js/pages/forms/basic-form-elements.js') }}"></script>
    <script src="{{ asset('material_template/js/pages/tables/jquery-datatable.js') }}"></script>
   <!--  <script src="{{ asset('material_template/js/pages/forms/editors.js') }}"></script> -->

    <!-- Autosize Plugin Js -->
    <script src="{{ asset('material_template/plugins/autosize/autosize.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('material_template/js/demo.js') }}"></script>

    <!-- Toastr notification pop-up -->
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script>
        @if(Session::has('success'))
            toastr.success("{{ Session::get('success') }}")
        @endif
        @if(Session::has('info'))
            toastr.info("{{ Session::get('info') }}")
        @endif
        @if(Session::has('error'))
            toastr.error("{{ Session::get('error') }}")
        @endif
    </script>

