    @include('includes.custom-js.confirmLogout-js')<!-- confirm logout form js-->
    @include('includes.modals.confirmLogout-form')
    <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{ asset('material_template/images/admin_male.png') }}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <?php
                    $fullname = Auth::user()->name;
                    ?>
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ $fullname }}
                    </div>
                    <div class="email">
                        <b>{{ strtoupper(trans(Auth::user()->role->role_name))}}</b>
                    </div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="{{ route('EditProfile') }}"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">help_outline</i>Help</a></li>
                            <li><a onclick="confirmLogout();"><i class="material-icons">input</i>Log Out</a></li>
                            <!-- <li><a href=" route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="material-icons">input</i>Log Out</a></li>
                            <form id="logout-form" action=" route('logout') }}" method="POST" style="display: none;">
                                 csrf_field() }}
                            </form> -->
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="
                            @if (Auth::user()->role->id == App\User::IS_ADMIN || Auth::user()->role->id == App\User::IS_SUPERADMIN)
                                {{ route('AdminDashboard') }}
                            @elseif (Auth::user()->role->id == App\User::IS_CASHIER)
                                {{ route('CashierDashboard') }}
                            @endif
                        ">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">security</i>
                            <span>Settings</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('EditProfile') }}">
                                    <i class="material-icons">account_circle</i>
                                    <span>My Account</span>
                                </a>
                            </li>
                            @if(Auth::user()->role->id == App\User::IS_SUPERADMIN)
                                <li>
                                    <a href="{{ route('users.index') }}">
                                        <i class="material-icons">account_circle</i>
                                        <span>Manage Users</span>
                                    </a>
                                </li>
                            @endif
                            @if(Auth::user()->role->id == App\User::IS_ADMIN || Auth::user()->role->id == App\User::IS_SUPERADMIN)
                                <li>
                                    <a href="#">
                                        <i class="material-icons">layers</i>
                                        <span>Audit Trail</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('reports.download') }}">
                                        <i class="material-icons">layers</i>
                                        <span>Download Reports</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>

                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">whatshot</i>
                            <span>Product Management</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('productgroups.index') }}">
                                    Product Groups
                                </a>
                            </li>
                            
                            <li>
                                <a href="{{ route('products.index') }}">
                                    Products
                                </a>
                            </li>
                            
                        </ul>
                    </li>

                    <li>
                        <a href="{{ route('stocks.index') }}">
                            <i class="material-icons">layers</i>
                            <span>Stocks Management</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('sell') }}">
                            <i class="material-icons">shopping_cart</i>
                            <span>Shopping Cart</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('transactions.index') }}">
                            <i class="material-icons">touch_app</i>
                            <span>Transactions</span>
                        </a>
                    </li>
                   
                    <li>
                        <a href="{{ route('reports.view') }}">
                            <i class="material-icons">assignment</i>
                            <span>Reports</span>
                        </a>
                    </li>

                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    WeBuff Solutions © Copyright 2018.
                </div>
                <div class="version">
                    All Rights Reserved.
                </div>
            </div>
            <!-- #Footer -->
    </aside>

