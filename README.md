 ## Project Setup/Configuration

### run on terminal:

> git clone https://bitbucket.org/webuffsolutions/emmans2018

> composer update

### rename .env.example with .env

### configure database on .env file

### run on terminal:

> alias pa="php artisan"

> pa migrate --seed / php artisan migrate --seed

> pa key:generate / php artisan key:generate

> pa config:clear / php artisan config:clear

### Admin default login:

username: admin
password: password

### Cashier default login:

username: cashier1 / cashier2
password: password
