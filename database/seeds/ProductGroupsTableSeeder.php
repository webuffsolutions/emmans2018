<?php

use Illuminate\Database\Seeder;

class ProductGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_groups')->insert([
            [
                'group_name' => 'A',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'group_name' => 'B',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
              	'group_name' => 'C',
              	'created_at' => date('Y-m-d H:i:s'),
              	'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
              	'group_name' => 'D',
              	'created_at' => date('Y-m-d H:i:s'),
              	'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
              	'group_name' => 'E',
              	'created_at' => date('Y-m-d H:i:s'),
              	'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
              	'group_name' => 'Others',
              	'created_at' => date('Y-m-d H:i:s'),
              	'updated_at' => date('Y-m-d H:i:s'),
            ],

      	]);
    }
}
