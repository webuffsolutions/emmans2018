<?php

Auth::routes();

route::post('/auth', 'AuthController@login')->name('AuthLogin');

route::get('user/logout', 'AuthController@logout')->name('logout1');

/*route::post('/logout', 'AuthController@logout')->name('logout1');*/

route::group(['middleware' => 'auth'], function() {

	route::get('/', 'DashboardController@AdminDashboard')->name('AdminDashboard');

	route::get('/cashier/dashboard', 'DashboardController@CashierDashboard')->name('CashierDashboard');

	//pop up modal upon cashier login
	route::get('/cashier/shift/selection', 'DashboardController@CashierShiftSelectionDashboard')
		->name('CashierShiftSelectionDashboard');

	route::get('/admin/dashboard', 'DashboardController@AdminDashboard')->name('AdminDashboard');

	//profile
	route::get('profile', 'ProfileController@EditProfile')->name('EditProfile');
	route::post('profile', 'ProfileController@UpdateProfile')->name('UpdateProfile');

	//users
	route::resource('users', 'UsersController');
	route::get('api/users', 'UsersController@apiUsers')->name('api.users');
	route::get('api/users/{id}', 'UsersController@edit')->name('api.users.edit');

	//product groups
	route::resource('productgroups', 'ProductGroupsController');
	route::get('api/productgroups', 'ProductGroupsController@apiProductGroups')->name('api.productgroups');

	//products
	route::resource('products', 'ProductsController');
	route::get('api/products', 'ProductsController@apiProducts')->name('api.products');

	//contacts
	route::resource('contact', 'ContactController');
	route::get('api/contact', 'ContactController@apiContact')->name('api.contact');

	//stocks / inventory
	route::get('stocks', 'StocksController@index')->name('stocks.index');
	route::get('api/stocks', 'StocksController@apiStocks')->name('api.stocks');
	route::get('stocks/add/{id}', 'StocksController@addStocks')->name('stocks.add'); //add stock view
	route::post('stocks/addStore/{id}', 'StocksController@addStocksStore')->name('stocks.store'); //add stock action
	route::get('stocks/less/{id}', 'StocksController@lessStocks')->name('stocks.less'); //less stock view
	route::post('stocks/lessStore/{id}', 'StocksController@lessStocksStore')->name('stocks.less.store'); //less stock action

	//sell and cart items
	route::get('sell', 'CartController@viewSellItems')->name('sell');
	//filter per category
	route::get('sell/filter/{group_id}', 'CartController@filterSellItems')->name('sell.filter');

	//filter all items
	route::get('sell/filter', 'CartController@searchSellItems')->name('sell.search');

	//add to cart 
	route::get('cart/add/{id}', 'CartController@addToCart')->name('cart.add');

	//add multiple to cart (view)
	route::get('cart/add/multi/{id}', 'CartController@addMultipleToCart')->name('cart.add.multiple');
	//store multiple to cart (Store)
	route::post('cart/store/multi/{id}', 'CartController@storeMultipleToCart')->name('cart.store.multiple');
	//deduct from cart
	route::get('cart/deduct/{cart_row_id}/{cart_product_id}/{cart_product_qty}', 'CartController@deductFromCart')->name('cart.deduct');
	//remove from cart
	route::get('cart/remove/{cart_row_id}/{cart_product_id}/{cart_product_qty}', 'CartController@removeFromCart')->name('cart.remove');
	//checkout
	route::post('cart/checkout', 'CartController@checkout')->name('cart.checkout');
	//empty cart
	route::post('cart/empty', 'CartController@emptyCart')->name('cart.empty');

	//transactions
	route::get('transactions', 'TransactionsController@index')->name('transactions.index');

	//edit transaction
	route::get('transactions/edit/{order_transactions_id}', 'TransactionsController@edit')->name('transactions.edit');

	//void order
	route::get('void/order/{product_id}/{qty}/{order_id}', 'VoidController@voidOrder')->name('void.order');

	//void order qty
	route::post('void/order/qty/{product_id}/{qty}/{order_id}', 'VoidController@voidOrderQty')->name('void.order.qty');

	//void transaction
	route::post('void/transaction/{transaction_id}', 'VoidController@voidTransaction')->name('void.transaction');

	//home
	route::get('/home', 'HomeController@index')->name('home');

	//view reports
	route::get('reports', 'ReportsController@index')->name('reports.view');

	//shift selection
	route::get('shift/selection', 'ShiftSelectionController@index')->name('shift.selection');

	//export pdf
	/*route::get('pdf/export', 'PdfController@exportPdf')->name('export.pdf');*/

	//downloadable reports
	route::get('reports/download', 'ReportsController@downloadableReports')->name('reports.download');

});//end routegroup


