<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalOrderTransaction extends Model
{
    protected $guarded = [];
}
