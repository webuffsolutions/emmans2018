<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalOrder extends Model
{
    protected $guarded = [];
    protected $table = 'total_orders';
}
