<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyInventory extends Model
{
    protected $guarded = [];
    protected $table = 'daily_inventories';
}
