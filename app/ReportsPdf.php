<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportsPdf extends Model
{
    protected $guarded = [];
    protected $table = 'reports_pdf';
}	
