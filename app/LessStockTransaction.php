<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessStockTransaction extends Model
{
    protected $table = 'less_stock_transactions';
    protected $guarded = [];
}
