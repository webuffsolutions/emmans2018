<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoidOrder extends Model
{
    protected $guarded = [];
    protected $table = 'void_orders';
}
