<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mockery\Exception;
use Yajra\DataTables\DataTables;
use App\Contact;
class ContactController extends Controller
{
    public function index()
    {
        //
        return view('system/contacts/index');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['photo'] = null;
        if ($request->hasFile('photo')){
            $input['photo'] = '/upload/photo/'.str_slug($input['name'], '-').'.'.$request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('/upload/photo/'), $input['photo']);
        }

        if($request->name == 'Denneth') {
                return response()->json([
                'error' => 'contactExists',
                'message' => 'Contact Already Exists!'
            ]);
        }
        
        Contact::create($input);

        return response()->json([
            'success' => true,
            'message' => 'Contact Created Successfully!'
        ]);
        
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $contact = Contact::findOrFail($id);
        return $contact;
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $contact = Contact::findOrFail($id);
        $input['photo'] = $contact->photo;
        if ($request->hasFile('photo')){
            if (!$contact->photo == NULL){
                unlink(public_path($contact->photo));
            }
            $input['photo'] = '/upload/photo/'.str_slug($input['name'], '-').'.'.$request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('/upload/photo/'), $input['photo']);
        }
        $contact->update($input);
        return response()->json([
            'success' => true,
            'message' => 'Contact Updated'
        ]);
    }

    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);
        if (!$contact->photo == NULL){
            unlink(public_path($contact->photo));
        }
        Contact::destroy($id);
        return response()->json([
            'success' => true,
            'message' => 'Contact Deleted'
        ]);
    }
    public function apiContact()
    {
        $contact = Contact::all();
 
        return Datatables::of($contact)
            ->addColumn('show_photo', function($contact){
                if ($contact->photo == NULL){
                    return 'No Image';
                }
                return '<img class="rounded-square" width="50" height="50" src="'. url($contact->photo) .'" alt="">';
            })
            ->addColumn('action', function($contact){
                return '<a onclick="editForm('. $contact->id .')" class="btn btn-primary btn-md">Edit</a> ' .
                       '<a onclick="deleteData('. $contact->id .')" class="btn btn-danger btn-md">Delete</a>';
            })
            ->rawColumns(['show_photo', 'action'])->make(true);
    }
}