<?php

namespace App\Http\Controllers;
use App\Order;
use App\Product;
use Auth, Session;
use App\ReportsPdf;
use App\ProductGroup;
use App\OrderTransaction;
use Illuminate\Http\Request;
use Dompdf\Dompdf;
use Dompdf\Options;
use PDF;
use DB;

class PdfController extends Controller
{
    /*public function exportPdf() {
    	//GROUP, PRODUCT, BEG, REQ, END, QTY SOLD, AMOUNT
    	$product_groups = ProductGroup::leftjoin('products', 'products.group_id', 'product_groups.id')
    		->leftjoin('orders', 'orders.product_id', 'products.id')
    		->select(DB::raw('SUM(orders.amount) as SALES'), 'product_groups.group_name as group_name')
    		->groupBy('product_groups.id')
    		->get();

    	//to get stocks orders
        $getBegFromPrev = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
            ->select('products.id as beg_product_id', 'daily_inventories.in_stock as beg')
            ->where('daily_inventories.status', 'previous')
            ->groupBy('products.id')
            ->get();

    	$reports = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
    		->join('product_groups', 'product_groups.id', 'products.group_id')
    		->leftjoin('stock_transactions', 'stock_transactions.product_id', 'products.id')
    		->select('product_groups.group_name as group_name', 'products.id as product_id', 'products.product_name', 'daily_inventories.in_stock as beg', DB::raw('SUM(stock_transactions.qty) as RQ'), 'daily_inventories.in_stock as end')
            ->where('daily_inventories.status', 'current')
    		->groupBy('products.id')
    		->get();

    	//to get orders
    	$reports1 = Product::leftjoin('orders', 'orders.product_id', 'products.id')
    		->select('products.id as prod_id', DB::raw('SUM(orders.qty) as qty_sold'), DB::raw('SUM(orders.amount) as total_amount'))
    		->groupBy('products.id')
    		->get();
    	
    	$total_discount = OrderTransaction::sum('discount');

    	$total_sales = Order::join('products', 'products.id', 'orders.product_id')
    		->join('product_groups', 'product_groups.id', 'products.group_id')
    		->select(DB::raw('SUM(orders.amount) as total'))
    		->where('product_groups.group_name', '!=', 'Others')
    		->first();

    	$export_pdf = PDF::loadView('system/reports-pdf/index', [
    		'product_groups' => $product_groups,
    		'getBegFromPrev' => $getBegFromPrev,
    		'reports' => $reports,
    		'reports1' => $reports1,
    		'total_discount' => $total_discount,
    		'total_sales' => $total_sales
    	]);

    	//return $export_pdf->stream('index.php');

    	$rand = str_random(3);

    	$name = 'reports-'.date('m-d-Y H:i:s A');

    	$export_pdf->save(public_path('/upload/pdf_reports/emmans_reports_'.date('m_d_Y H_i').'_'.$rand.'.pdf'));

    	$reports_pdf_save_db = ReportsPdf::create([
            'pdf_file' => 'upload/pdf_reports/emmans_reports_'.date('m_d_Y H_i').'_'.$rand.'.pdf',
            'name' => $name,
        ]);
        //END PDF

    }*/
}
