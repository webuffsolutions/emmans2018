<?php

namespace App\Http\Controllers;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\ProductGroup;
use Session, Auth, App\User;

class ProductGroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('system/product-groups/index');
    }

    public function store(Request $request)
    {
        $check_existing_productgroup = ProductGroup::where('group_name', $request->group_name)->first();

        if($check_existing_productgroup){
                return response()->json([
                'error' => 'productGroupExists',
                'message' => 'Product Group Already Exists!'
            ]);
        }

        $input = ProductGroup::create([
            'group_name' => $request->group_name
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Product Group Created Successfully!'
        ]);
    }

    public function edit($id)
    {
        $productgroup = ProductGroup::find($id);
        return $productgroup;
    }

    public function update(Request $request, $id)
    {
        $productgroup = ProductGroup::findOrFail($id);
        $exists = ProductGroup::where('group_name', $request->group_name)
            ->where('group_name', '!=', $productgroup->group_name)->first();

        if($exists){
            return response()->json([
                'error' => 'productGroupExsistsUpdate',
                'message' => 'Product Group exists!'
            ]);
        }

        $productgroup->group_name = $request->group_name;
        $productgroup->save();

        return response()->json([
            'success' => true,
            'message' => 'Product Group Updated'
        ]);
    }

    public function destroy($id)
    {
        $productgroup = ProductGroup::findOrFail($id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Product Group Deleted'
        ]);
    }

    public function apiProductGroups() 
    {
        $product_groups = ProductGroup::all();

        return Datatables::of($product_groups)
        
        ->addColumn('action', function($product_groups){
                if (Auth::user()->role->id == User::IS_SUPERADMIN){
                    return '<a onclick="editProductGroupForm('. $product_groups->id .')" class="btn btn-primary btn-md">Edit</a> ' .
                           '<a onclick="deleteProductGroupData('. $product_groups->id .')" class="btn btn-danger btn-md">Delete</a>';
                }else{
                    return '<a onclick="" class="btn btn-primary btn-md" disabled>Edit</a>' .
                       ' <a onclick="" class="btn btn-danger btn-md" disabled>Delete</a>';
                }
                
            })
            ->rawColumns(['action'])->make(true);
    }
}
