<?php
namespace App\Http\Controllers;
use App\Order;
use App\VoidOrder;
use App\DailyInventory;
use App\OrderTransaction;
use App\StockTransaction;
use Illuminate\Http\Request;
use App\LessStockTransaction;
use DB;

class ShiftSelectionController extends Controller
{
    public function index(Request $request)
    {
    	if ($request->shift == 'AM'){
    		if ($request->shift_option == 'new') {
    			//execute
    			$daily_inventories = DailyInventory::where('status', 'current')->get();
        		$prev_daily_inventories = DailyInventory::where('status', 'previous')->get();

		        //from current
		        foreach($daily_inventories as $daily_inventory){
		           $product_id[] = $daily_inventory->product_id;
		           $in_stock[] = $daily_inventory->in_stock;
		        }

        		for($i=0;$i<count($product_id);$i++){
            		//set previous to passed
            		if($prev_daily_inventories){
                		DailyInventory::where('product_id', $product_id[$i])->where('status', 'previous')
                		->update([
                    		'in_stock' => $in_stock[$i], 'status' => 'passed'
                		]);
            		}
            		//set current to previous
            		DailyInventory::where('product_id', $product_id[$i])->where('status', 'current')
            		->update([
                		'in_stock' => $in_stock[$i],
                		'status' => 'previous'
            		]);
            		//create current
            		DailyInventory::create([
                		'product_id' => $product_id[$i], 'in_stock' => $in_stock[$i], 'shift' => 'AM', 'status' => 'current'
            		]);
                    
                    DB::table('less_stock_transactions')->delete();
                    DB::table('order_transactions')->delete();
                    DB::table('orders')->delete();
                    DB::table('stock_transactions')->delete();
                    DB::table('void_orders')->delete();
		        }

    			return response()->json([
            		'success' => 'true',
            		'message' => 'AM SHIFT / NEW SHIFT!'
        		]);

    		} else if ($request->shift_option == 'resume') {
    			return response()->json([
            		'success' => 'true',
            		//'message' => 'AM SHIFT / RESUME SHIFT!'
        		]);
    		}
    		
    	} else if ($request->shift == 'PM') {
    		if ($request->shift_option == 'new') {

    			//execute
    			$daily_inventories = DailyInventory::where('status', 'current')->get();
        		$prev_daily_inventories = DailyInventory::where('status', 'previous')->get();

		        //from current
		        foreach($daily_inventories as $daily_inventory){
		           $product_id[] = $daily_inventory->product_id;
		           $in_stock[] = $daily_inventory->in_stock;
		        }

        		for($i=0;$i<count($product_id);$i++){
            		//set previous to passed
            		if($prev_daily_inventories){
                		DailyInventory::where('product_id', $product_id[$i])->where('status', 'previous')
                		->update([
                    		'in_stock' => $in_stock[$i], 'status' => 'passed'
                		]);
            		}
            		//set current to previous
            		DailyInventory::where('product_id', $product_id[$i])->where('status', 'current')
            		->update([
                		'in_stock' => $in_stock[$i],
                		'status' => 'previous'
            		]);
            		//create current
            		DailyInventory::create([
                		'product_id' => $product_id[$i], 'in_stock' => $in_stock[$i], 'shift' => 'PM', 'status' => 'current'
            		]);

                    DB::table('less_stock_transactions')->delete();
                    DB::table('order_transactions')->delete();
                    DB::table('orders')->delete();
                    DB::table('stock_transactions')->delete();
                    DB::table('void_orders')->delete();

		        }

    			return response()->json([
            		'success' => 'true',
            		'message' => 'PM SHIFT / NEW SHIFT!'
        		]);

    		} else if ($request->shift_option == 'resume') {
    			return response()->json([
            		'success' => 'true',
            		'message' => 'PM SHIFT / RESUME SHIFT!'
        		]);
    		}
    	}
    	//
    }
}
