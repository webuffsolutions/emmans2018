<?php

namespace App\Http\Controllers;
use App\Order;
use App\TotalOrder;
use App\DailyInventory;
use App\OrderTransaction;
use Cart, Auth, Session, DB;
use Illuminate\Http\Request;
use App\TotalOrderTransaction;
use App\ProductGroup, App\Product;

class CartController extends Controller
{
   	public function viewSellItems()
   	{
   		$product_groups = ProductGroup::all();
   		$products = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
   			->select('products.id as id', 'products.product_name as product_name', 'products.image as image',
   					'daily_inventories.in_stock as in_stock')
        ->where('daily_inventories.status', 'current')
   			->paginate(8);

   		return view('system/cart/index', [
   			'product_groups' => $product_groups,
   			'products' => $products
   		]);
   	}

   	public function filterSellItems(Request $request, $group_id)
   	{
   		$product_groups = ProductGroup::all();
   		$products = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
   			->select('products.id as id', 'products.product_name as product_name', 'products.image as image',
   					'daily_inventories.in_stock as in_stock')
   			->where('products.group_id', $group_id)
        ->where('daily_inventories.status', 'current')
   			->paginate(8);

   		return view('system/cart/index', [
   			'product_groups' => $product_groups,
   			'products' => $products
   		]);
   	}

   	public function searchSellItems(Request $request)
   	{
   		if($request->has('product')){
   			$product_groups = ProductGroup::all();
   			$products = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
   			->select('products.id as id', 'products.product_name as product_name', 'products.image as image',
   					'daily_inventories.in_stock as in_stock')
   			->where('products.product_name', 'LIKE', "%$request->product%")
        ->where('daily_inventories.status', 'current')
   			->paginate(8);

   		} else {
   			$product_groups = ProductGroup::all();
   			$products = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
   			->select('products.id as id', 'products.product_name as product_name', 'products.image as image',
   					'daily_inventories.in_stock as in_stock')
        ->where('daily_inventories.status', 'current')
   			->paginate(8);
   		}

   		return view('system/cart/index', [
   			'product_groups' => $product_groups,
   			'products' => $products
   		]);
   	}

    public function addToCart(Request $request, $id)
    {
        $product = Product::join('product_groups', 'product_groups.id', 'products.group_id')
            ->select('products.*', 'product_groups.group_name as group_name')
            ->where('products.id', $id)
            ->first();

        $daily_inventory = DailyInventory::where('product_id', $id)
          ->where('status', 'current')
          ->first();

        $product_order_qty = 1;

        if($product_order_qty > $daily_inventory->in_stock && $product->group_name !== 'E'){
            if($product_order_qty > $daily_inventory->in_stock && $product->group_name !== 'Others'){
               Session::flash('error', 'Not enough stocks');
               return redirect()->back();
            }
        }
        //push product items to cart
        $cartItem = Cart::add([
            'id' => $product->id, 'name' => $product->product_name,
            'qty' => $product_order_qty, 'price' => $product->price,
        ]);

        // Deduct product qty from inventory database
        if($product->group_name !== 'E'){
            if($product->group_name !== 'Others'){
                DailyInventory::where('product_id', $product->id)
                  ->where('status', 'current')
                  ->decrement('in_stock', $product_order_qty);
            }
        }
        return redirect()->back();
    }

    public function addMultipleToCart(Request $request, $id)
    {
        $product = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
            ->select('products.id as id', 'products.product_name as product_name', 'daily_inventories.in_stock as in_stock')
            ->where('products.id', $id)
            ->where('daily_inventories.status', 'current')
            ->first();

        return $product;
    }

    public function storeMultipleToCart(Request $request, $id)
    {
        $product = Product::join('product_groups', 'product_groups.id', 'products.group_id')
            ->select('products.*', 'product_groups.group_name as group_name')
            ->where('products.id', $id)
            ->first();

        $daily_inventory = DailyInventory::where('product_id', $id)->where('status', 'current')->first();

        if($request->qty > $daily_inventory->in_stock && $product->group_name !== 'E'){
            if($request->qty > $daily_inventory->in_stock && $product->group_name !== 'Others'){
                return response()->json([
                    'error' => 'notEnoughStocks',
                    'message' => 'Not enough stocks'
                ]);
            }
        }

        //push product items to cart
        $cartItem = Cart::add([
            'id' => $product->id, 'name' => $product->product_name,
            'qty' => $request->qty, 'price' => $product->price
        ]);

        // Deduct product qty from inventory database
        if($product->group_name !== 'E'){
            if($product->group_name !== 'Others'){
                DailyInventory::where('product_id', '=', $product->id)
                  ->where('status', 'current')
                  ->decrement('in_stock', $request->qty);   
            }
        }

        return response()->json([
            'success' => true,
            'message' => 'Added multiple qty!'
        ], 201);

    }

    public function checkout(Request $request)
    {
        if(Cart::content() -> count() == 0){
            Session::flash('error', 'Cart is empty!');
            return redirect()->back();
        }

        $this->validate($request, [
            'scd' => 'required'
        ]);

        $cartItems = Cart::content(); // content of the cart 
        $subtotal = Cart::subtotal(); //total amt of all cart products

        //insert to order_transactions table
        $order_transaction = OrderTransaction::create([
            'type' => 'sell',
            'discount' => $request->scd,
            'user_id' => Auth::user()->id
        ]);

        //insert to total_order_transactions table
        $total_order_transaction = TotalOrderTransaction::create([
            'type' => 'sell',
            'discount' => $request->scd,
            'user_id' => Auth::user()->id
        ]);

        foreach($cartItems as $cartItem)
        {
            Order::create([
                'product_id' => $cartItem->id,
                'qty' => $cartItem->qty,
                'amount' => $cartItem->total,
                'transaction_id' => $order_transaction->id,
                'user_id' => Auth::user()->id
            ]);

            TotalOrder::create([
              'product_id' => $cartItem->id,
              'qty' => $cartItem->qty,
              'amount' => $cartItem->total,
              'transaction_id' => $total_order_transaction->id,
              'user_id' => Auth::user()->id
            ]);

        }

        Cart::destroy(); //empty the cart session
        Session::flash('success', 'Products Sold!');
        return redirect()->back();

    }

    public function deductFromCart($cart_row_id, $cart_product_id, $cart_product_qty)
    {
        $product = Product::join('product_groups', 'product_groups.id', 'products.group_id')
            ->select('products.*', 'product_groups.group_name as group_name')
            ->where('products.id', $cart_product_id)
            ->first();

        $daily_inventory = DailyInventory::where('product_id', $cart_product_id)->where('status', 'current')->first();
        $deduct_cart = Cart::update($cart_row_id, $cart_product_qty - 1); //deduct 1 qty from cart product

        if(!$deduct_cart){
            //if error
            return redirect()->back();
        }

        //return/increment product qty to inventory table
        if($product->group_name !== 'E'){
            if($product->group_name !== 'Others'){
                DailyInventory::where('product_id', '=', $cart_product_id)->where('status', 'current')->increment('in_stock', 1);
            }
        }

        return redirect()->back();

    }

    public function removeFromCart($cart_row_id, $cart_product_id, $cart_product_qty)
    {
        //dd($cart_row_id, $cart_product_id, $cart_product_qty);
        $product = Product::join('product_groups', 'product_groups.id', 'products.group_id')
            ->select('products.*', 'product_groups.group_name as group_name')
            ->where('products.id', $cart_product_id)
            ->first();

        $daily_inventory = DailyInventory::where('product_id', $cart_product_id)->where('status', 'current')->first();

        //return/increment product qty to inventory table
        if($product->group_name !== 'E'){
            if($product->group_name !== 'Others'){
                DailyInventory::where('product_id', '=', $cart_product_id)
                  ->where('status', 'current')
                  ->increment('in_stock', $cart_product_qty);
            }
        }

        Cart::remove($cart_row_id);
        Session::flash('success', 'Product removed from cart');
        return redirect()->back();

    }

    //this function is used to empty the cart and return all the items and qty to the inventory
    public function emptyCart(Request $request)
    {
        if(Cart::content() -> count() == 0){
            Session::flash('error', 'Cart is already empty!');
            return redirect()->back();
        }
        $cartDetails = Cart::content(); // content of the cart 
        foreach($cartDetails as $cart){
            $prod_id[] = $cart->id;
            $qty[] = $cart->qty;
        }
        for($i=0;$i<count($prod_id);$i++){
            DailyInventory::where('product_id', $prod_id[$i])->where('status', 'current')->increment('in_stock', $qty[$i]);
        }
        Cart::destroy(); //empty the cart session
        Session::flash('success', 'Cart is now empty!');
        return redirect()->back();
    }

}
