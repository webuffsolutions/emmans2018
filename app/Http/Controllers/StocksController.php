<?php
namespace App\Http\Controllers;
use Auth;
use App\Product;
use App\DailyInventory;
use App\StockTransaction;
use Illuminate\Http\Request;
use App\LessStockTransaction;
use Yajra\DataTables\DataTables;

class StocksController extends Controller
{
    public function index()
    {
    	return view('system/stocks/index');
    }

    public function addStocks($id)
    {
        $product = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
            ->select('products.id as id', 'products.product_name as product_name', 'daily_inventories.in_stock as in_stock')
            ->where('products.id', $id)
            ->where('daily_inventories.status', 'current')
            ->first();

        return $product;
    }

    public function addStocksStore(Request $request, $id)
    {
        $increment_stocks = DailyInventory::where('product_id', $id)
            ->where('status', 'current')
            ->increment('in_stock', $request->qty);

        $update_inventory_shift = DailyInventory::where('product_id', $id)
            ->where('status', 'current')
            ->first();

        $update_inventory_shift->shift = 'AM';
        $update_inventory_shift->save();

        $insert_stock_transaction = StockTransaction::create([
            'type' => 'restock',
            'product_id' => $id,
            'qty' => $request->qty,
            'user_id' => Auth::user()->id
        ]);

        return response()->json([
            'success' => true, 'message' => 'Restock success'
        ]);
    }

    public function lessStocks($id)
    {
        $product = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
            ->select('products.id as id', 'products.product_name as product_name', 'daily_inventories.in_stock as in_stock')
            ->where('products.id', $id)
            ->where('daily_inventories.status', 'current')
            ->first();

        return $product;
    }

    public function lessStocksStore(Request $request, $id)
    {

        $inv = DailyInventory::where('product_id', $id)
            ->where('status', 'current')
            ->first();

        if($request->qty > $inv->in_stock){
            return response()->json([
                'error' => 'greaterThanInstock',
                'message' => 'Quantity is greater than current in-stock'
            ]);
        }

        $decrement_stocks = DailyInventory::where('product_id', $id)
            ->where('status', 'current')
            ->decrement('in_stock', $request->qty);

        $insert_less_stock_transaction = LessStockTransaction::create([
            'type' => 'less',
            'product_id' => $id,
            'qty' => $request->qty,
            'user_id' => Auth::user()->id
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Less stock success'
        ]);
    }

    public function apiStocks()
    {
        $product = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
            ->select('products.id as id', 'products.product_name as product_name', 'daily_inventories.in_stock as in_stock')
            ->where('daily_inventories.status', 'current')
            ->get();
 
        return Datatables::of($product)
            ->addColumn('in_stock', function($product){
                if($product->in_stock <= 10){
                    return '<font color="red"><b>'.$product->in_stock.'</b></font>';
                }else{
                    return $product->in_stock;
                }
                
            })
            ->addColumn('action', function($product){
                return '<a onclick="addStockForm('. $product->id .')" class="btn btn-primary btn-md">Add</a> ' .
                       '<a onclick="lessStockForm('. $product->id .')" class="btn btn-danger btn-md">Less</a>';
            })
            ->rawColumns(['in_stock', 'action'])->make(true);
    }
}
