<?php

namespace App\Http\Controllers;
use Auth;
use App\Order;
use App\Product;
use App\VoidOrder;
use App\TotalOrder;
use App\DailyInventory;
use App\OrderTransaction;
use Illuminate\Http\Request;
use App\TotalOrderTransaction;

class VoidController extends Controller
{
    public function voidOrder(Request $request, $product_id, $qty, $order_id)
    {
    	$order = Order::where('id', $order_id)->first();
    	$count = Order::where('transaction_id', $order->transaction_id)->get()->count();

        $total_order = TotalOrder::where('id', $order_id)->first();
        $count1 = TotalOrder::where('transaction_id', $total_order->transaction_id)->get()->count();

    	if($count == 1){
    		$void_order = Order::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)->delete();
    		$delete_order_transaction = OrderTransaction::where('id', $order->transaction_id)->delete();

            //tables for monthly reports of sales
            $void_total_order = TotalOrder::where('product_id', $product_id)->where('transaction_id', $total_order->transaction_id)->delete();
            $delete_total_order_transaction = TotalOrderTransaction::where('id', $total_order->transaction_id)->delete();
            //
    		$return_instock = DailyInventory::where('product_id', $product_id)->increment('in_stock', $qty);

    		$insert_void_order_table = VoidOrder::create([
    			'transaction_id' => $order->transaction_id,
    			'product_id'     => $product_id, 
                'qty'            => $qty,
    			'user_id'        => Auth::user()->id, 
                'reason'         => $request->reason
    		]);
    	} else {
    		$void_order = Order::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)->delete();
            $void_total_order = TotalOrder::where('product_id', $product_id)->where('transaction_id', $total_order->transaction_id)->delete();

    		$return_instock = DailyInventory::where('product_id', $product_id)->increment('in_stock', $qty);
    		$insert_void_order_table = VoidOrder::create([
    			'transaction_id' => $order->transaction_id,
    			'product_id'     => $product_id,
    			'qty'            => $qty,
    			'user_id'        => Auth::user()->id,
    			'reason'         => $request->reason
    		]);

    		return response()->json([
            	'success' => 'true',
            	'message' => 'Order voided'
        	]);
    	}

    }

    public function voidOrderQty(Request $request, $product_id, $qty, $order_id)
    {
    	$order = Order::where('id', $order_id)->first();
    	$count = Order::where('transaction_id', $order->transaction_id)->get()->count();
    	$product = Product::where('id', $product_id)->first();

    	//if void order qty is greater than the qty of the product order
    	if($request->void_qty > $qty){
    		return response()->json([
        		'error'   => 'invalidVoidOrderQty',
        		'message' => 'Quantity must not be greater than the current order quantity'
    		]);
    	}

    	//kapag isa nalang yung order qty..
    	if($qty == 1 && $count > 1){
    		$void_order_qty = Order::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)->delete();
            $void_total_order_qty = TotalOrder::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)->delete();

    		$return_instock = DailyInventory::where('product_id', $product_id)->increment('in_stock', 1);

    		return response()->json([
        		'success' => 'true',
        		'message' => 'Order quantity voided'
    		]);

    	} else if($qty == 1) {
    		//kapag isa nalang ung order qty and isa nalang din ung order na may same transaction_id
    		$void_order_qty = Order::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)->delete();
            $void_total_order_qty = TotalOrder::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)->delete();

    		$delete_order_transaction = OrderTransaction::where('id', $order->transaction_id)->delete();
            $delete_total_order_transaction = TotalOrderTransaction::where('id', $order->transaction_id)->delete();

    		$return_instock = DailyInventory::where('product_id', $product_id)->increment('in_stock', 1);
    		$insert_void_order_table = VoidOrder::create([
    			'transaction_id' => $order->transaction_id,
    			'product_id'     => $product_id,
    			'qty'            => $qty,
    			'user_id'        => Auth::user()->id,
    			'reason'         => $request->reason
    		]);

    		return response()->json([
        		'error'   => 'success',
        		'message' => 'Order and Transaction voided'
    		]);

    	} else if($qty == $request->void_qty){
    		//if product qty == requested void qty and orders with the same transaction id is = 1
    		$void_order_qty = Order::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)->delete();
            $void_total_order_qty = TotalOrder::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)->delete();

    		if ($count == 1) {
    			$delete_order_transaction = OrderTransaction::where('id', $order->transaction_id)->delete();
                $delete_total_order_transaction = TotalOrderTransaction::where('id', $order->transaction_id)->delete();
    		}

    		$return_instock = DailyInventory::where('product_id', $product_id)->increment('in_stock', $request->void_qty);

    		$insert_void_order_table = VoidOrder::create([
    			'transaction_id' => $order->transaction_id,
    			'product_id'     => $product_id,
    			'qty'            => $request->void_qty,
    			'user_id'        => Auth::user()->id,
    			'reason'         => $request->reason
    		]);

    		return response()->json([
        		'success' => 'true',
        		'message' => 'Order quantity voided'
    		]);

    	} else {
    		$void_order_qty = Order::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)
    		->decrement('qty', $request->void_qty);

            $void_total_order_qty = TotalOrder::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)
            ->decrement('qty', $request->void_qty);

    		$total_void_amount = $request->void_qty * $product->price;

    		$void_order_amount = Order::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)
    		->decrement('amount', $total_void_amount);

            $void_total_order_amount = TotalOrder::where('product_id', $product_id)->where('transaction_id', $order->transaction_id)
            ->decrement('amount', $total_void_amount);

    		$return_instock = DailyInventory::where('product_id', $product_id)->increment('in_stock', $request->void_qty);

    		$insert_void_order_table = VoidOrder::create([
    			'transaction_id' => $order->transaction_id,
    			'product_id'     => $product_id,
    			'qty'            => $request->void_qty,
    			'user_id'        => Auth::user()->id,
    			'reason'         => $request->reason
    		]);

    		return response()->json([
        		'success' => 'true',
        		'message' => 'Order quantity voided'
    		]);
    	}
    	
    }

    public function voidTransaction(Request $request, $transaction_id)
    {
        $orders = Order::where('transaction_id', $transaction_id)->get();

        foreach($orders as $order){
            $prod_id[]  = $order->product_id;
            $qty[]      = $order->qty;
            $trans_id[] = $order->transaction_id;
        }

        //delete array of products where transaction_id is = $transaction_id
        Order::whereIn('product_id', $prod_id)->where('transaction_id', $trans_id)->delete();
        TotalOrder::whereIn('product_id', $prod_id)->where('transaction_id', $trans_id)->delete();
        //delete transaction
        OrderTransaction::where('id', $transaction_id)->delete();
        TotalOrderTransaction::where('id', $transaction_id)->delete();
        //return inventory in_stock
        for($i=0;$i<count($prod_id);$i++){
            DailyInventory::where('product_id', $prod_id[$i])->increment('in_stock', $qty[$i]);

            $insert_void_tbl = VoidOrder::create([
                'transaction_id' => $transaction_id,
                'product_id'     => $prod_id[$i],
                'qty'            => $qty[$i],
                'user_id'        => Auth::id(),
                'reason'         => $request->reason
            ]);
        }

    	return response()->json([
            'success' => 'true',
            'message' => 'Transaction voided'
        ]);
    }
}
