<?php
namespace App\Http\Controllers;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Product;
use App\DailyInventory;
use App\User;
use Session, Auth;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('system/products/index');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $check_existing_product = Product::where('product_name', $request->product_name)->first();

        if($check_existing_product){
                return response()->json([
                'error' => 'productExists',
                'message' => 'Product Already Exists!'
            ]);
        }

        //$input['image'] = null;
        if ($request->hasFile('image')){
            $input['image'] = '/upload/products/'.str_slug($input['product_name'], '-').'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('/upload/products/'), $input['image']);
        } 

        $product = Product::create([
            'product_name' => $request->product_name,
            'group_id' => $request->group_id,
            'image' => $input['image'],
            'price' => $request->price
        ]);

        $new_daily_inventory = DailyInventory::create([
            'product_id' => $product->id,
            'in_stock' => 0,
            'shift' => 'AM'
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Product Created Successfully!'
        ], 201);
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id)->toArray();
        return response()->json(['product' => $product], 200);
        //$user= User::where('role_id', 1)->first()->toArray();
        //return response()->json(['product' => $product, 'user' => $user], 200);
        //return $product;
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $exists = Product::where('product_name', $request->product_name)
            ->where('product_name', '!=', $product->product_name)->first();

        if($exists){
            return response()->json([
                'error' => 'productExsistsUpdate',
                'message' => 'Product already exists!'
            ]);
        }
        $input = $request->all();
        $input['image'] = $product->image;
        if ($request->hasFile('image')){
            if (!$product->image == NULL){
                unlink(public_path($product->image));
            }
            $input['image'] = '/upload/products/'.str_slug($input['product_name'], '-').'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('/upload/products/'), $input['image']);
        }

        $product->product_name = $request->product_name;
        $product->image = $input['image'];
        $product->group_id = $request->group_id;
        $product->price = $request->price;
        $product->save();

        return response()->json([
            'success' => true,
            'message' => 'Product Updated'
        ], 200);
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        if (!$product->image == NULL){
            unlink(public_path($product->image));
        }
        Product::destroy($id);
        $delete_from_daily_inventory = DailyInventory::where('product_id', $id)->delete();
        return response()->json(['success' => true, 'message' => 'Product Deleted'], 200);
    }

    public function apiProducts() 
    {
        $product = Product::join('product_groups', 'product_groups.id', 'products.group_id')
            ->select('products.id as id', 'products.product_name as product_name', 'products.image as image',
                    'product_groups.group_name as group_name', 'products.price as price')
            ->get();
 
        return Datatables::of($product)
            ->addColumn('image', function($product){
                if ($product->image == NULL){
                    return 'No Image';
                }
                return '<img class="rounded-square" width="50" height="50" src="'. url($product->image) .'" alt="">';
            })
            ->addColumn('price', function($product){
                return 'P'.$product->price;
            })
            ->addColumn('action', function($product){

                if(Auth::user()->role->id == User::IS_SUPERADMIN){
                    return '<a onclick="editProductForm('. $product->id .')" class="btn btn-primary btn-md">Edit</a> ' .
                       '<a onclick="deleteProductData('. $product->id .')" class="btn btn-danger btn-md">Delete</a>';
                }else{
                    return '<a onclick="" class="btn btn-primary btn-md" disabled>Edit</a>' .
                       ' <a onclick="" class="btn btn-danger btn-md" disabled>Delete</a>';
                }
                
            })
            ->rawColumns(['image', 'action'])->make(true);
    }
}
