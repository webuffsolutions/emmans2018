<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
	//popup modal for shift selection
	public function CashierShiftSelectionDashboard(){
    	return view('system/dashboard/shiftSelection-dashboard');
    }

    public function CashierDashboard () {
    	return view('system/dashboard/cashier-dashboard');
    }

    public function AdminDashboard () {
    	return view('system/dashboard/admin-dashboard');
    }

}
