<?php

namespace App\Http\Controllers;
use Session;
use App\Order;
use App\OrderTransaction;
use App\StockTransaction;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    public function index()
    {
    	$order_transactions = OrderTransaction::join('users', 'users.id', 'order_transactions.user_id')
    		->select('order_transactions.id as trans_id', 'order_transactions.type as type', 'users.name as user',
    				'order_transactions.created_at as created_at')
    		->get();

    	$stock_transactions = StockTransaction::join('users', 'users.id', 'stock_transactions.user_id')
    		->select('stock_transactions.id as trans_id', 'stock_transactions.type as type', 'users.name as user',
    				'stock_transactions.created_at as created_at')
    		->get();

    	return view('system/transactions/index', [
    		'order_transactions' => $order_transactions,
    		'stock_transactions' => $stock_transactions
    	]);
    }

    public function edit($order_transactions_id)
    {
        $orders = OrderTransaction::join('orders', 'orders.transaction_id', 'order_transactions.id')
            ->join('users', 'users.id', 'orders.user_id')
            ->join('products', 'products.id', 'orders.product_id')
            ->select('order_transactions.id as transaction_id', 'users.name as cashier', 
                    'order_transactions.created_at as created_at', 'products.id as product_id', 
                    'products.product_name as product_name', 
                    'orders.id as order_id', 'orders.qty as qty', 'orders.amount as amount', 
                    'order_transactions.discount as discount')
            ->where('order_transactions.id', $order_transactions_id)
            ->get();

        return view('system/transactions/editOrderTransaction')->with('orders', $orders);
        //return $orders;
    }
}
