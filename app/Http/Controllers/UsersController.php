<?php

namespace App\Http\Controllers;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\User;
use Session;
use Auth;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('system/users/index');
    }

    public function store(Request $request)
    {
        $check_existing_user = User::where('name', $request->name)->first();
        $check_existing_username = User::where('username', $request->username)->first();

        if($check_existing_user || $check_existing_username){
                return response()->json([
                'error' => 'userExists',
                'message' => 'User Already Exists!'
            ]);
        }
        //User::create($input);
        $input = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'role_id' => $request->role_id,
            'password' => bcrypt('password')
        ]);

        return response()->json([
            'success' => true,
            'message' => 'User Created Successfully!'
        ]);
        
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return $user;
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user = User::findOrFail($id);
        $user->update($input);
        return response()->json([
            'success' => true,
            'message' => 'User Updated'
        ]);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'User Deleted'
        ]);
    }

    public function apiUsers() 
    {
        $users = User::join('roles', 'roles.id', 'users.role_id')
            ->select('users.id as id', 'users.name as name', 'users.username as username', 'roles.role_name as role')
            ->where('roles.id', '!=', User::IS_SUPERADMIN)
            ->get();

        return Datatables::of($users)
        ->addColumn('role', function($users){
                if ($users->role == NULL){
                    return 'No Role';
                }
                return $users->role;
            })
        ->addColumn('action', function($users){
                if ($users->id == Auth::user()->id){
                    return '<a onclick="editUserForm('. $users->id .')" class="btn btn-primary btn-md">Edit</a>';
                }
                return '<a onclick="editUserForm('. $users->id .')" class="btn btn-primary btn-md">Edit</a> ' .
                       '<a onclick="deleteUserData('. $users->id .')" class="btn btn-danger btn-md">Delete</a>';
            })
            ->rawColumns(['role','action'])->make(true);
    }
}
