<?php

namespace App\Http\Controllers;
use Auth, DB;
use App\Order;
use App\ReportsPdf;
use App\OrderTransaction;
use Illuminate\Http\Request;
use App\ProductGroup, App\Product, App\DailyInventory;


class ReportsController extends Controller
{
    public function index()
    {
    	//GROUP, PRODUCT, BEG, REQ, END, QTY SOLD, AMOUNT
    	$product_groups = ProductGroup::leftjoin('products', 'products.group_id', 'product_groups.id')
    		->leftjoin('orders', 'orders.product_id', 'products.id')
    		->select(DB::raw('SUM(orders.amount) as SALES'), 'product_groups.group_name as group_name')
    		->groupBy('product_groups.id')
    		->get();

    	//to get stocks orders

        $getBegFromPrev = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
            ->select('products.id as beg_product_id', 'daily_inventories.in_stock as beg')
            ->where('daily_inventories.status', 'previous')
            ->groupBy('products.id')
            ->get();

    	$reports = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
    		->join('product_groups', 'product_groups.id', 'products.group_id')
    		->leftjoin('stock_transactions', 'stock_transactions.product_id', 'products.id')
    		->select('product_groups.group_name as group_name', 'products.id as product_id', 'products.product_name', 'daily_inventories.in_stock as beg', DB::raw('SUM(stock_transactions.qty) as RQ'), 'daily_inventories.in_stock as end')
            ->where('daily_inventories.status', 'current')
    		->groupBy('products.id')
    		->get();

    	//to get orders
    	$reports1 = Product::leftjoin('orders', 'orders.product_id', 'products.id')
    		->select('products.id as prod_id', DB::raw('SUM(orders.qty) as qty_sold'), DB::raw('SUM(orders.amount) as total_amount'))
    		->groupBy('products.id')
    		->get();
    	
    	$total_discount = OrderTransaction::sum('discount');

    	$total_sales = Order::join('products', 'products.id', 'orders.product_id')
    		->join('product_groups', 'product_groups.id', 'products.group_id')
    		->select(DB::raw('SUM(orders.amount) as total'))
    		->where('product_groups.group_name', '!=', 'Others')
    		->first();

        $auth_user = Auth::user()->name;

    	return view('system/reports/index', [
    		'product_groups' => $product_groups,
    		'reports' => $reports, 'reports1' => $reports1,
    		'total_discount' => $total_discount, 'total_sales' => $total_sales,
            'getBegFromPrev' => $getBegFromPrev,
            'auth_user' => $auth_user
    	]);
    }

    public function downloadableReports()
    {
        $downloadableReports = ReportsPdf::all();
        return view('system/reports/downloadableReports')
            ->with('downloadableReports', $downloadableReports);
    }
}
