<?php

namespace App\Http\Controllers;
use Auth, DB;
use Session;
use App\User, App\Product;
use App\Order;
use App\VoidOrder;
use App\ProductGroup;
use App\DailyInventory;
use App\OrderTransaction;
use App\StockTransaction;
use Illuminate\Http\Request;
use App\LessStockTransaction;
use App\ReportsPdf;
use PDF;

class AuthController extends Controller
{
    public function login(Request $request){
    	if(Auth::attempt([
    		'username' => $request->username,
    		'password' => $request->password
    	]))
    	{
            //update daily_inventories

    		$user = User::where('username', $request->username)->first();

    		if($user->role->role_name == 'admin'){
    			return redirect()->route('AdminDashboard');
    		}
    		else if($user->role->role_name == 'cashier'){
    			return redirect()->route('CashierShiftSelectionDashboard');
    		}
            else if ($user->role->role_name == 'superadmin'){
                return redirect()->route('AdminDashboard');
            }
    	}
    	// if username/password is not equal
        Session::flash('info', 'Incorrect Username/Password');
        return redirect()->back();
    }

    public function logout(Request $request) {
        //GROUP, PRODUCT, BEG, REQ, END, QTY SOLD, AMOUNT
        $product_groups = ProductGroup::leftjoin('products', 'products.group_id', 'product_groups.id')
            ->leftjoin('orders', 'orders.product_id', 'products.id')
            ->select(DB::raw('SUM(orders.amount) as SALES'), 'product_groups.group_name as group_name')
            ->groupBy('product_groups.id')
            ->get();

        //to get stocks orders
        $getBegFromPrev = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
            ->select('products.id as beg_product_id', 'daily_inventories.in_stock as beg')
            ->where('daily_inventories.status', 'previous')
            ->groupBy('products.id')
            ->get();

        $reports = Product::join('daily_inventories', 'daily_inventories.product_id', 'products.id')
            ->join('product_groups', 'product_groups.id', 'products.group_id')
            ->leftjoin('stock_transactions', 'stock_transactions.product_id', 'products.id')
            ->select('product_groups.group_name as group_name', 'products.id as product_id', 'products.product_name', 'daily_inventories.in_stock as beg', DB::raw('SUM(stock_transactions.qty) as RQ'), 'daily_inventories.in_stock as end')
            ->where('daily_inventories.status', 'current')
            ->groupBy('products.id')
            ->get();

        //to get orders
        $reports1 = Product::leftjoin('orders', 'orders.product_id', 'products.id')
            ->select('products.id as prod_id', DB::raw('SUM(orders.qty) as qty_sold'), DB::raw('SUM(orders.amount) as total_amount'))
            ->groupBy('products.id')
            ->get();
        
        $total_discount = OrderTransaction::sum('discount');

        $total_sales = Order::join('products', 'products.id', 'orders.product_id')
            ->join('product_groups', 'product_groups.id', 'products.group_id')
            ->select(DB::raw('SUM(orders.amount) as total'))
            ->where('product_groups.group_name', '!=', 'Others')
            ->first();

        $export_pdf = PDF::loadView('system/reports-pdf/index', [
            'product_groups' => $product_groups, 'getBegFromPrev' => $getBegFromPrev,
            'reports' => $reports, 'reports1' => $reports1,
            'total_discount' => $total_discount, 'total_sales' => $total_sales
        ]);

        $rand = str_random(3);
        $name = 'reports-'.Auth::user()->username.'-'.date('m-d-Y H:i:s A');
        $export_pdf->save(public_path('/upload/pdf_reports/emmans_reports_'.date('m_d_Y H_i').'_'.$rand.'.pdf'));
        $reports_pdf_save_db = ReportsPdf::create([
            'pdf_file' => 'upload/pdf_reports/emmans_reports_'.date('m_d_Y H_i').'_'.$rand.'.pdf',
            'name' => $name,
        ]);
        //END PDF

        $cart = collect(session()->get('cart'));
        $destination = \Auth::logout();
        if (!config('cart.destroy_on_logout')) {
            $cart->each(function($rows, $identifier) {
                session()->put('cart.' . $identifier, $rows);
            });
        }

        return redirect()->to($destination);
        
    }
}
