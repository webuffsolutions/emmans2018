<?php
namespace App\Http\Controllers;
use Auth;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function EditProfile() {
    	return view ('system/profile/EditProfile');
    }

    public function UpdateProfile(Request $request) {

        $this->validate($request, [
            'name' => 'required', 'username' => 'required'
        ]);

        $thisUser = Auth::user();

        if($request->new_password && !$request->old_password) {
            Session::flash('error', 'Please enter old password');
            return redirect()->back();
        } else if ($request->old_password && !$request->new_password) {
            Session::flash('error', 'Please enter and confirm your new password.');
            return redirect()->back();
        }

        if($request->old_password){
            if (Hash::check(Input::get('old_password'), Auth::user()->password)) {
                if($request->new_password) {
                    $thisUser->password = bcrypt($request->new_password);
                }
            } else {
                Session::flash('error', 'incorrect password!');
                return redirect()->back();
            }
        }
        
    	$thisUser->name = $request->name;
    	$thisUser->username = $request->username;
    	$thisUser->save();

    	Session::flash('success', 'Profile updated');
    	return redirect()->back();
    }
}
