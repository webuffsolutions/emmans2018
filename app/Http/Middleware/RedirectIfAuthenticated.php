<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(Auth::user()->role->role_name == 'admin'){
                return redirect()->route('AdminDashboard');
            }
            else if(Auth::user()->role->role_name == 'cashier'){
                return redirect()->route('CashierDashboard');
            }
            return redirect()->back();
        }

        return $next($request);
    }
}
